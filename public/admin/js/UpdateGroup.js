/**
 * Created by Cesaer on 01/06/2017.
 */
$(document).ready(function(){
    $('label.expandable').closest('li').children('ul').hide();
    $('label.expandable').click(function() {
        $(this).closest('li').children('ul').toggle();
        return false;
    });
    $('.main').change(function () {
        if ($(this).prop("checked")==true) {
            $(this).closest('.parent').find('input[type=checkbox]:nth-child(1)').prop("checked", true);
        }
        else
        {
            $(this).closest('.parent').find('input[type=checkbox]:nth-child(1)').prop("checked", false);
        }
    });
    $('select').change(function(){
        $('label.expandable').closest('li').children('ul').show();
        $('input[type=checkbox]').each(function () {
            $(this).prop("checked", false);
        });
        // var data = data + "&cmd=select_group";
        var data = "&id=" + $(this).val();
        var url = $(this).data('route');
        $.post(url, data, function (res) {
            for ($i = 0; $i < res.length; $i++) {
                $('input[type=checkbox]').each(function () {
                    if ($(this).val() == res[$i]['name']) {
                        $(this).prop("checked", true);
                        return false;
                    }
                })
            }
        }, 'json');

    });

});
