<?php

namespace Database\Seeders;

use App\Models\Variable;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Variable::create(array('name'=>'email'));
        Variable::create(array('name'=>'phone'));
        Variable::create(array('name'=>'address'));
        Variable::create(array('name'=>'about_ar'));
        Variable::create(array('name'=>'about_en'));
        Variable::create(array('name'=>'facebook'));
        Variable::create(array('name'=>'whatsup'));
        Variable::create(array('name'=>'instagram'));
        Variable::create(array('name'=>'twitter'));
        Variable::create(array('name'=>'linkedin'));
        Variable::create(array('name'=>'youtube'));
    }
}
