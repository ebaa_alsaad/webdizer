<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class CreateAdminUserSeeder extends Seeder{

    public function run()
    {
        $user = User::create([
            'name' => 'ebaa',
            'email' => 'ebaa@admin.com',
            'password' => bcrypt('123456'),
            'role_name' => 'Admin',
            'status' => '1'
        ]);

        $role = Role::create(['name' => 'Admin']);

        $permissions = Permission::pluck('id','id')->all();
        $role->syncPermissions($permissions);
        $user->assignRole([$role->id]);

        $role = Role::create(['name' => 'Writer']);
        $role = Role::create(['name' => 'Editor']);
    }
}

