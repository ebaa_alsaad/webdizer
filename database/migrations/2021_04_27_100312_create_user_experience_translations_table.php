<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserExperienceTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_experience_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_experience_id')->unsigned();
            $table->string('locale')->index();
            $table->string('name');
            $table->text('text')->nullable();

            $table->unique(['user_experience_id', 'locale']);
            $table->foreign('user_experience_id')->references('id')->on('user_experiences')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_experience_translations');
    }
}
