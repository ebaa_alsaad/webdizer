<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class CategoryTranslation extends Model implements Searchable
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable=['name'];
    public function Category()
    {
        return $this->belongsTo(Category::class,'category_id');
    }

    public function getSearchResult(): SearchResult
    {
        $url = url('project', $this->id);

        return new SearchResult(
            $this,
            $this->name,
            $url
        );
    }
}
