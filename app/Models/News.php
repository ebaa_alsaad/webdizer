<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    public $translatedAttributes = ['title', 'text'];
    protected $fillable=['image','status','CategoryID'];
    public function NewsTranslation()
    {
        return $this->hasMany(NewsTranslation::class,'news_id');
    }
    public function Category()
    {
        return $this->belongsTo(Category::class, 'CategoryID');
    }
    public function scopeSelect($query)
    {
        return $query->orderBy('id','DESC')->get();
    }
    public function getStatus()
    {
        return $this->status == '1' ? 'مفعل' : 'غير مفعل' ;
    }
}
