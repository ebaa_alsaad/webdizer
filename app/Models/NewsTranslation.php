<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class NewsTranslation extends Model implements Searchable
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable=['title','text'];
    public function News()
    {
        return $this->belongsTo(News::class,'news_id');
    }
    public function getSearchResult(): SearchResult
    {
        $url = route('search_news', $this->id);
        $text = $this->text;

        return new SearchResult(
            $this,
            $this->title,
//            $this->text,
//            $this->created_at,
            $url
        );
    }
}
