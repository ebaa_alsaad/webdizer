<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    public $translatedAttributes = ['name', 'text' , 'job'];
    protected $fillable=['image', 'facebook' , 'twitter' , 'linkedin' , 'youtube'];
    public function TeamTranslation()
    {
        return $this->hasMany(TeamTranslation::class,'team_id');
    }
    public function scopeSelect($query)
    {
        return $query->orderBy('id','DESC');
    }
}
