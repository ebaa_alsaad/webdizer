<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    public $translatedAttributes = ['title', 'text'];
    protected $fillable=['image' , 'CategoryID'];
    public function ProjectTranslation()
    {
        return $this->hasMany(ProjectTranslation::class,'project_id');
    }
    public function Category()
    {
        return $this->belongsTo(Category::class,'CategoryID');
    }
    public function scopeSelect($query)
    {
        return $query->orderBy('id','DESC');
    }
}
