<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    public $translatedAttributes = ['name'];
    protected $fillable=['type'];
    public function CategoryTranslation()
    {
        return $this->hasMany(CategoryTranslation::class,'category_id');
    }
    public function Project()
    {
        return $this->hasMany(Project::class,'CategoryID');
    }
    public function News()
    {
        return $this->hasMany(News::class,'CategoryID');
    }
}
