<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class ServiceTranslation extends Model implements Searchable
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable=['title','text'];
    public function getSearchResult(): SearchResult
    {
        $url = route('search_service', $this->id);

        return new SearchResult(
            $this,
            $this->title,
            $url
        );
    }
    public function Service()
    {
        return $this->belongsTo(Service::class,'service_id');
    }
}
