<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AllRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name:ar' => 'required',
            'name:en' => 'required',
            'text:ar' => 'required',
            'text:en' => 'required',
            'title:ar' => 'required',
            'title:en' => 'required',
            'image'   => 'mimes:jpg,jpeg,png',
        ];
    }
    public function messages()
    {
        return [
            'required' => 'هذا الحقل مطلوب',
            'image.mimes' => 'يجب أن يكون امتداد الصورة jpg,jpeg,png',
        ];
    }
}
