<?php

namespace App\Http\Controllers;

use App\Http\Requests\AllRequest;
use App\Models\Category;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    function __construct()
    {

        $this->middleware('permission:الأخبار| عرض الأخبار', ['only' => ['index']]);
        $this->middleware('permission:إضافة خبر جديد', ['only' => ['create','store']]);
        $this->middleware('permission:تعديل خبر', ['only' => ['edit','update']]);
        $this->middleware('permission:حذف خبر', ['only' => ['destroy']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $new = News::select();;
        return view('admin.news.index' , compact('new'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::get();
        return view('admin.news.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if(!$request->has('status'))
                $request->request->add(['status'=>0]);

            $validation =  $this->validate( $request, [
                'title:ar' => 'required',
                'title:en' => 'required',
                'text:ar' => 'required',
                'text:en' => 'required',
                'CategoryID' => 'required',
                'image' => 'mimes:jpg,jpeg,png',
                'status' => 'required',
            ]);
            $filePath = "";
            if ($request->has('image')) {

                $filePath = uploadImage('news', $request->image);
            }
            $news = News::create([
                'title:ar' => $request -> {'title:ar'},
                'title:en' => $request -> {'title:en' },
                'text:ar' => $request -> {'text:ar'},
                'text:en' => $request -> {'text:en' },
                'CategoryID' => $request -> CategoryID,
                'status' => $request -> status,
                'image' => $filePath
            ]);
            return redirect()->back()->with(['success'=>'تم الحفظ بنجاح']);
        }catch (\Exception $ex){
            return redirect()->back()->with(['error'=>'حدث خطأ ما']);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $news= News::where('status',1)->select();
        $categories = Category::with('News')
            ->where('type', '2')->get();
        return view('front.news.index',compact('news','categories'));

    }
    public function show_detail($id){
        $news= News::find($id);
        return view('front.news.detail' , compact('news'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        $news = News::select()->find($news->id);
        $category = Category::get();
        if(!$news){
            return redirect()->route('Admin.news')->with(['error'=>'هذا الخبر غير موجود']);
        }
        return view('admin.news.edit',compact('news' , 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ID)
    {

        try {
            $news = News::where('id', $ID)->first();
            $validation =  $this->validate( $request, [
                'title:ar'   => 'required',
                'title:en'   => 'required',
                'text:ar'    => 'required',
                'text:en'    => 'required',
                'CategoryID' => 'required',
                'image'      => 'mimes:jpg,jpeg,png',
            ]);
            if(!$request->has('status'))
                $request->request->add(['status'=>0]);
            else
                $request->request->add(['status'=>1]);

            //save image
            if ($request->has('image')) {
                $Path = deleteImage('news', $news->image);
                $filePath = uploadImage('news', $request->image);
            }
            else
                $filePath = $news->image;

            $news->update([
                'title:ar' => $request -> {'title:ar'},
                'title:en' => $request -> {'title:en' },
                'text:ar' => $request -> {'text:ar'},
                'text:en' => $request -> {'text:en' },
                'CategoryID' => $request -> CategoryID,
                'status' => $request -> status,
                'image' => $filePath
            ]);

            return redirect('Admin/news')->with(['success'=>'تم التحديث بنجاح']);
        }
        catch(\Exception $e){
            return redirect()->back()->with("error", 'حدث خطأ اثناء التعديل');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $new  = News::find($request->delet_id);
            if (!$new ) {
                return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
            }
            $Path = deleteImage('news', $new->image);
            $new ->delete();

            return redirect()->back()->with('success','تم الحذف بنجاح');

        } catch (\Exception $ex) {
            return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
        }
    }
}

