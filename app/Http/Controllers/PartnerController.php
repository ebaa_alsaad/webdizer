<?php

namespace App\Http\Controllers;

use App\Models\Partner;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partners = Partner::select()->paginate(PAGINATION_COUNT);
        return view('admin.partner.index' , compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partner.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validation =  $this->validate( $request, [
                'link' => 'required',
                'image' => 'mimes:jpg,jpeg,png',
            ]);
            $filePath = "";
            if ($request->has('image')) {

                $filePath = uploadImage('partner', $request->image);
            }

            $partners = Partner::create([
                'link' => $request -> link,
                'image' => $filePath
            ]);
            return redirect()->back()->with(['success'=>'تم الحفظ بنجاح']);
        }catch (\Exception $ex){
            return redirect()->back()->with(['error'=>'حدث خطأ ما']);

        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function show(Partner $partner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function edit(Partner $partner)
    {
        $partner = Partner::find($partner->id);
        if(!$partner){
            return redirect()->route('Admin.partners')->with(['error'=>'هذا الشخص غير موجود']);
        }
        return view('admin.partner.edit',compact('partner' ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $Id)
    {
        try {
            $partner = Partner::where('id', $Id)->first();
            $validation =  $this->validate( $request, [
                'link' => 'required',
                'image' => 'mimes:jpg,jpeg,png',
            ]);
            //save image
            if ($request->has('image')) {
                $Path = deleteImage('partner', $partner->image);
                $filePath = uploadImage('partner', $request->image);
            }
            else
                $filePath = $partner->image;

            $partner->update([
                'link' => $request -> link,
                'image' => $filePath
            ]);

            return redirect('Admin/partners')->with(['success'=>'تم التحديث بنجاح']);
        }
        catch(\Exception $e){
            return redirect()->back()->with("error", 'حدث خطأ اثناء التعديل');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $partner = Partner::find($request->delet_id);
            if (!$partner) {
                return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
            }
            $Path = deleteImage('slider', $partner->image);
            $partner->delete();

            return redirect()->back()->with('success','تم الحذف بنجاح');

        } catch (\Exception $ex) {
            return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
        }
    }
}

