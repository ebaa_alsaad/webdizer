<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
    public function index()
    {
        $contacts = Contact::select();;
        return view('admin.con.index' , compact('contacts'));
    }
    // Create Contact Form
    public function create() {
        return view('front.contact');
    }

    // Store Contact Form data
    public function sendEmail(Request $request)
    {

        $request->validate([
            'email' => 'required|email',
            'subject' => 'required',
            'name' => 'required',
            'msg' => 'required',
        ]);
        $data = [
            'subject' => $request->subject,
            'name' => $request->name,
            'email' => $request->email,
            'msg' => $request->msg
        ];

        Mail::send('front.mail', $data, function($message) use ($data) {
            $message->from($data['email']);
            $message->to('thewebdizer@gmail.com')
                ->subject('THE WEBDIZER Website Contact Form');
        });
        Contact::create($request->all());
        return back()->with(['message' => 'Email successfully sent!']);
    }
}
