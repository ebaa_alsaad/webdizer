<?php

namespace App\Http\Controllers;

use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class TeamController extends Controller
{
    function __construct()
    {

        $this->middleware('permission:فريق العمل|عرض الأعضاء', ['only' => ['index']]);
        $this->middleware('permission:إضافة جديد', ['only' => ['create','store']]);
        $this->middleware('permission:تعديل شخص', ['only' => ['edit','update']]);
        $this->middleware('permission:حذف شخص', ['only' => ['destroy']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::select()->paginate(PAGINATION_COUNT);
        return view('admin.team.index' , compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.team.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validation =  $this->validate( $request, [
                'name:ar' => 'required',
                'name:en' => 'required',
                'text:ar' => 'required',
                'text:en' => 'required',
                'image' => 'mimes:jpg,jpeg,png',
            ]);
            $filePath = "";
            if ($request->has('image')) {

                $filePath = uploadImage('team', $request->image);
            }

            $team = Team::create([
                'name:ar'  => $request -> {'name:ar'},
                'name:en'  => $request -> {'name:en' },
                'text:ar'  => $request -> {'text:ar'},
                'text:en'  => $request -> {'text:en' },
                'job:ar'   => $request -> {'job:ar'},
                'job:en'   => $request -> {'job:en' },
                'facebook' => $request -> facebook,
                'twitter'  => $request -> twitter,
                'linkedin' => $request -> linkedin,
                'youtube'  => $request -> youtube,
                'image'    => $filePath
            ]);

            return redirect()->back()->with(['success'=>'تم الحفظ بنجاح']);
        }catch (\Exception $ex){
            return redirect()->back()->with(['error'=>'حدث خطأ ما']);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $teams = Team::get();
        for($i=0;$i<count($teams);$i++)
        {
            if($i%2==0)
                $teams[$i]['class']='left';
            else
                $teams[$i]['class']='right';

        }
        return view('front.team.index' , compact('teams'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        $teams = Team::select()->find($team->id);
        if(!$teams){
            return redirect()->route('Admin.team')->with(['error'=>'هذا الشخص غير موجود']);
        }
        return view('admin.team.edit',compact('teams' ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ID)
    {
        try {
            $team = Team::where('id', $ID)->first();
            $validation =  $this->validate( $request, [
                'name:ar' => 'required',
                'name:en' => 'required',
                'text:ar' => 'required',
                'text:en' => 'required',
                'image' => 'mimes:jpg,jpeg,png',
            ]);
            //save image
            if ($request->has('image')) {
                $Path = deleteImage('team', $team->image);
                $filePath = uploadImage('team', $request->image);
            }
            else
                $filePath = $team->image;

            $team->update([
                'name:ar'  => $request -> {'name:ar'},
                'name:en'  => $request -> {'name:en' },
                'text:ar'  => $request -> {'text:ar'},
                'text:en'  => $request -> {'text:en' },
                'job:ar'   => $request -> {'job:ar'},
                'job:en'   => $request -> {'job:en' },
                'facebook' => $request -> facebook,
                'twitter'  => $request -> twitter,
                'linkedin' => $request -> linkedin,
                'youtube'  => $request -> youtube,
                'image'    => $filePath
            ]);

            return redirect('Admin/team')->with(['success'=>'تم الحفظ بنجاح']);
        }catch (\Exception $ex){
            return redirect()->back()->with(['error'=>'حدث خطأ ما']);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $teams = Team::find($request->delet_id);
            if (!$teams) {
                return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
            }
            $Path = deleteImage('team', $teams->image);

            $teams->delete();

            return redirect()->back()->with('success','تم الحذف بنجاح');

        } catch (\Exception $ex) {
            return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
        }
    }
}
