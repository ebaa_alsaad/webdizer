<?php

namespace App\Http\Controllers;

use App\Models\CategoryTranslation;
use App\Models\NewsTranslation;
use App\Models\ProjectTranslation;
use App\Models\ServiceTranslation;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        return view('admin.dashboard');
    }
    public function home(){
        return view('front.index');
    }
    public function index_search(Request $request){
        $searchterm =  '0' ;
        $searchResults = (new Search())
            ->perform($searchterm);
        return view('front.search', compact('searchResults', 'searchterm'));
    }
    public function search(Request $request)
    {
        $searchterm = $request->input('query');
        $searchResults = (new Search())
            ->registerModel(ProjectTranslation::class, ['text','title'])
            ->registerModel(NewsTranslation::class,['text','title'])
            ->registerModel(ServiceTranslation::class,['text','title'])
            ->perform($searchterm) ;
        return view('front.search', compact('searchResults', 'searchterm'));
    }
}


