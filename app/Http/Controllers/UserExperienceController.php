<?php

namespace App\Http\Controllers;

use App\Models\UserExperience;
use Illuminate\Http\Request;

class UserExperienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userExperiences = UserExperience::select()->paginate(PAGINATION_COUNT);
        return view('admin.UserExperience.index' , compact('userExperiences'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.UserExperience.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validation =  $this->validate( $request, [
                'name:ar' => 'required',
                'name:en' => 'required',
                'text:ar' => 'required',
                'text:en' => 'required',
            ]);

            $userExperiences = UserExperience::create([
                'name:ar'  => $request -> {'name:ar'},
                'name:en'  => $request -> {'name:en' },
                'text:ar'  => $request -> {'text:ar'},
                'text:en'  => $request -> {'text:en' },
            ]);

            return redirect()->back()->with(['success'=>'تم الحفظ بنجاح']);
        }catch (\Exception $ex){
            return redirect()->back()->with(['error'=>'حدث خطأ ما']);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserExperience  $userExperience
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserExperience  $userExperience
     * @return \Illuminate\Http\Response
     */
    public function edit(UserExperience $userExperience)
    {
        $userExperiences = UserExperience::select()->find($userExperience->id);
        if(!$userExperiences){
            return redirect()->route('Admin.UserExperience')->with(['error'=>'هذه التجربة غير موجودة']);
        }
        return view('admin.UserExperience.edit',compact('userExperiences' ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserExperience  $userExperience
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $ID)
    {

        try {
            $userExperience = UserExperience::where('id', $ID)->first();
            $validation =  $this->validate( $request, [
                'name:ar' => 'required',
                'name:en' => 'required',
                'text:ar' => 'required',
                'text:en' => 'required',
            ]);
            $userExperience->update([
                'name:ar'  => $request -> {'name:ar'},
                'name:en'  => $request -> {'name:en' },
                'text:ar'  => $request -> {'text:ar'},
                'text:en'  => $request -> {'text:en' },
            ]);

            return redirect('Admin/userExperience')->with(['success'=>'تم الحفظ بنجاح']);
        }catch (\Exception $ex){
            return redirect()->back()->with(['error'=>'حدث خطأ ما']);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserExperience  $userExperience
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        try {
            $userExperience = UserExperience::find($request->delet_id);
            if (!$userExperience) {
                return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
            }

            $userExperience->delete();

            return redirect()->back()->with('success','تم الحذف بنجاح');

        } catch (\Exception $ex) {
            return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
        }
    }
}

