<?php

namespace App\Http\Controllers;

use App\Models\MapLocation;
use App\Models\Variable;
use Illuminate\Http\Request;

class VariableController extends Controller
{
//    function __construct()
//    {
//
//        $this->middleware('permission:حول الشركة', ['only' => ['getAbout']]);
//        $this->middleware('permission:تعديل حول الشركة', ['only' => ['create']]);
//        $this->middleware('permission:تعديل صلاحية', ['only' => ['setAbout']]);
//        $this->middleware('permission:معلومات التواصل', ['only' => ['getInfo','setInfo']]);
//
//    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $about_ar = Variable::where('name','about_ar')->first();
        $about_en = Variable::where('name','about_en')->first();
         return view('admin.about.edit', compact('about_en','about_ar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Variable  $variable
     * @return \Illuminate\Http\Response
     */
    public function show(Variable $variable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Variable  $variable
     * @return \Illuminate\Http\Response
     */
    public function edit(Variable $variable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Variable  $variable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Variable $variable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Variable  $variable
     * @return \Illuminate\Http\Response
     */
    public function destroy(Variable $variable)
    {
        //
    }
    public function getMap(){
        $mapAddr = MapLocation::get();
        return view('admin.map.create',compact('mapAddr'));
    }
    public function getAbout(){
        $about_ar = Variable::where('name','about_ar')->first();
        $about_en = Variable::where('name','about_en')->first();
        return view('admin.about.create',compact('about_ar','about_en'));
    }
    public function setAbout(Request $request){
        try{
            $about_ar = Variable::where('name','about_ar')->first();
            $about_en = Variable::where('name','about_en')->first();

            $about_ar->update(['value:ar'=>$request->{'about:ar'}]);
            $about_en->update(['value:en'=>$request->{'about:en'}]);
            return redirect('Admin/about')->with('success','تم التعديل بنجاح');

        }
        catch(\Exception $e){
            return redirect()->back()->with('error','هناك خطا ما يرجي المحاوله فيما بعد');
        }
    }
    public function getInfo(){
        $variable=Variable::all();
        return view('admin.information.index',compact('variable'));
    }
    public function setInfo(Request $request){
        try {
            if ($request->email) {
                $v = Variable::where('name', '=', 'email')->first();
                if ($request->email == '*') {
                    $v->{'value:ar'} = null;
                    $v->{'value:en'} = null;
                } else {
                    $v->{'value:ar'} = $request->email;
                    $v->{'value:en'} = $request->email;
                }
                $v->save();
            }
            if ($request->phone) {
                $v = Variable::where('name', '=', 'phone')->first();
                if ($request->phone == '*') {
                    $v->{'value:ar'} = null;
                    $v->{'value:en'} = null;
                } else {
                    $v->{'value:ar'} = $request->phone;
                    $v->{'value:en'} = $request->phone;
                }
                $v->save();
            }
            if ($request->facebook) {
                $v = Variable::where('name', '=', 'facebook')->first();
                if ($request->facebook == '*') {
                    $v->{'value:ar'} = null;
                    $v->{'value:en'} = null;
                } else {
                    $v->{'value:ar'} = $request->facebook;
                    $v->{'value:en'} = $request->facebook;
                }
                $v->save();
            }
            if ($request->whatsup) {

                $v = Variable::where('name', '=', 'whatsup')->first();
                if ($request->whatsup == '*') {
                    $v->{'value:ar'} = null;
                    $v->{'value:en'} = null;
                } else {
                    $v->{'value:ar'} = $request->whatsup;
                    $v->{'value:en'} = $request->whatsup;
                }
                $v->save();
//            dd($v->value,$v);
            }
            if ($request->instagram) {
                $v = Variable::where('name', '=', 'instagram')->first();
                if ($request->instagram == '*') {
                    $v->{'value:ar'} = null;
                    $v->{'value:en'} = null;
                } else {
                    $v->{'value:ar'} = $request->instagram;
                    $v->{'value:en'} = $request->instagram;
                }
                $v->save();

                //dd($v->value);
            }
            if ($request->twitter) {
                $v = Variable::where('name', '=', 'twitter')->first();
                if ($request->twitter == '*') {
                    $v->{'value:ar'} = null;
                    $v->{'value:en'} = null;
                } else {
                    $v->{'value:ar'} = $request->twitter;
                    $v->{'value:en'} = $request->twitter;
                }
                $v->save();

                //dd($v->value);
            }
            if ($request->linkedin) {
                $v = Variable::where('name', '=', 'linkedin')->first();
                if ($request->linkedin == '*') {
                    $v->{'value:ar'} = null;
                    $v->{'value:en'} = null;
                } else {
                    $v->{'value:ar'} = $request->linkedin;
                    $v->{'value:en'} = $request->linkedin;
                }
                $v->save();

                //dd($v->value);
            }
            if ($request->youtube) {
                $v = Variable::where('name', '=', 'youtube')->first();
                if ($request->youtube == '*') {
                    $v->{'value:ar'} = null;
                    $v->{'value:en'} = null;
                } else {
                    $v->{'value:ar'} = $request->youtube;
                    $v->{'value:en'} = $request->youtube;
                }
                $v->save();

                //dd($v->value);
            }
            if ($request->address_ar) {
                $v = Variable::where('name', '=', 'address')->first();
                if ($request->address_ar == '*') {
                    $v->{'value:ar'} = null;
                } else {
                    $v->{'value:ar'} = $request->address_ar;
                }
                $v->save();

                //dd($v->value);
            }
            if ($request->address_en) {
                $v = Variable::where('name', '=', 'address')->first();
                if ($request->address_en == '*') {
                    $v->{'value:en'} = null;
                } else {
                    $v->{'value:en'} = $request->address_en;
                }
                $v->save();

                //dd($v->value);
            }
            $response = array(
                'success' => true,
                'msg' =>'تم التعديل بنجاح'
            );
        }
        catch(\Exception $e){
            $response = array(
                'success' => false,
                'msg' => $e->getMessage()
            );
        }
        return response()->json($response);
    }

}

