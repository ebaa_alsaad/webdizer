@extends('layouts.site')

@section('title')
    {{__('app.Team')}}
@endsection
@section('content')
    <div id="team" role="main" class="main "  @if(LaravelLocalization::getCurrentLocale()=='ar') dir="rtl" style="text-align: right" @else dir="ltr" @endif>
        <section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7" style="background-image: url({{asset('front/img/team.jpg')}});">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12 align-self-center p-static order-2 text-center">
                        <h1 class="text-12">{{__('app.Meet')}} {{__('app.Our Team')}}</h1>
                    </div>
                </div>
            </div>
        </section>

{{--        <div class="container py-4">--}}
{{--            @foreach($teams as $team)--}}
{{--                @if($team->class=='left')--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-7 order-2">--}}
{{--                    <div>--}}
{{--                        <h2 class="text-color-dark font-weight-bold text-8 mb-0 pt-0 mt-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="300">--}}
{{--                            {{$team->name}}</h2>--}}
{{--                    </div>--}}
{{--                    <div class="overflow-hidden mb-3">--}}
{{--                        <p class="font-weight-bold text-primary text-uppercase mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="500">{{$team->job}}</p>--}}
{{--                    </div>--}}
{{--                    <p class="pb-3 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800">{{$team->text}}</p>--}}
{{--                    <hr class="solid my-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="900">--}}
{{--                    <div class="row align-items-center appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1000">--}}
{{--                        <div class="col-sm-6 text-lg-right my-4 my-lg-0">--}}
{{--                            <ul class="social-icons @if(LaravelLocalization::getCurrentLocale()=='ar') float-lg-right @else float-lg-left @endif ">--}}
{{--                                @isset($team->facebook)--}}
{{--                                <li class="social-icons-facebook"><a href="{{$team->facebook}}" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>--}}
{{--                                @endisset--}}
{{--                                @isset($team->twitter)--}}
{{--                                <li class="social-icons-twitter"><a href="{{$team->twitter}}" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>--}}
{{--                                @endisset--}}
{{--                                @isset($team->linkedin)--}}
{{--                                <li class="social-icons-linkedin"><a href="{{$team->linkedin}}" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>--}}
{{--                                @endisset--}}
{{--                                @isset($team->youtube)--}}
{{--                                <li class="social-icons-youtube"><a href="{{$team->youtube}}" target="_blank" title="youtube"><i class="fab fa-youtube"></i></a></li>--}}
{{--                                @endisset--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-5 order-md-2 mb-4 mb-lg-0 appear-animation" data-appear-animation="fadeInRightShorter">--}}
{{--                    <img @if($team->image != null) src="{{asset("admin/{$team->image}")}}" @else  src="{{asset("front/img/default.jpg")}}" @endif class="img-fluid"  alt="">--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="row">--}}
{{--                <div class="col">--}}
{{--                    <hr class="solid my-5">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--                @else--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-5 order-md-2 mb-4 mb-lg-0 appear-animation" data-appear-animation="fadeInLeftShorter">--}}
{{--                    <img @if($team->image != null) src="{{asset("admin/{$team->image}")}}" @else  src="{{asset("front/img/default.jpg")}}" @endif  class="img-fluid"  alt="">--}}
{{--                </div>--}}
{{--                <div class="col-md-7 order-2">--}}
{{--                    <div class="overflow-hidden">--}}
{{--                        <h2 class="text-color-dark font-weight-bold text-8 mb-0 pt-0 mt-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="300">--}}
{{--                            {{$team->name}}</h2>--}}
{{--                    </div>--}}
{{--                    <div class="overflow-hidden mb-3">--}}
{{--                        <p class="font-weight-bold text-primary text-uppercase mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="500">--}}
{{--                            {{$team->job}}</p>--}}
{{--                    </div>--}}
{{--                    <p class="pb-3 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800">{{$team->text}}</p>--}}
{{--                    <hr class="solid my-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="900">--}}
{{--                    <div class="row align-items-center appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1000">--}}
{{--                        <div class="col-sm-6 text-lg-right my-4 my-lg-0">--}}
{{--                            <ul class="social-icons  @if(LaravelLocalization::getCurrentLocale()=='ar') float-lg-right @else float-lg-left @endif ">--}}
{{--                                @isset($team->facebook)--}}
{{--                                    <li class="social-icons-facebook"><a href="{{$team->facebook}}" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>--}}
{{--                                @endisset--}}
{{--                                @isset($team->twitter)--}}
{{--                                    <li class="social-icons-twitter"><a href="{{$team->twitter}}" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>--}}
{{--                                @endisset--}}
{{--                                @isset($team->linkedin)--}}
{{--                                    <li class="social-icons-linkedin"><a href="{{$team->linkedin}}" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>--}}
{{--                                @endisset--}}
{{--                                @isset($team->youtube)--}}
{{--                                    <li class="social-icons-youtube"><a href="{{$team->youtube}}" target="_blank" title="youtube"><i class="fab fa-youtube"></i></a></li>--}}
{{--                                @endisset--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="row">--}}
{{--                <div class="col">--}}
{{--                    <hr class="solid my-5">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--                @endif--}}
{{--            @endforeach--}}

{{--        </div>--}}
        <div class="container mb-5">

            <div id="team_pd" class="row p-5 mb-5 justify-content-center appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">
            @foreach($teams as $team)
                <div class="col-sm-6 col-lg-4 mb-4 mb-5">
                <span class="thumb-info thumb-info-hide-wrapper-bg thumb-info-no-zoom">
                    <span class="thumb-info-wrapper">
                        <a href="{{url('team')}}">
                            <img style="width: 100% !important;"  class="img-fluid" @if($team->image != null)src="{{asset("admin/{$team->image}")}}" @else
                            src="{{asset("front/img/default.jpg")}}" @endif alt=""/>
                        </a>
                    </span>
                    <span class="thumb-info-caption">
                        <h3 class="font-weight-extra-bold text-color-dark text-4 line-height-1 mt-3 mb-0">{{$team->name}}</h3>
                        <span class="text-2 mb-0">{{$team->job}}</span>
                        <span class="thumb-info-caption-text pt-1">
                            {!!  \Illuminate\Support\Str::limit( $team->text, 100) !!}
                            </span>
                        <span class="thumb-info-social-icons">
                            @isset($team->facebook)
                                <a title="Facebook" target="_blank" href="{{$team->facebook}}"><i class="fab fa-facebook-f"></i><span> </span></a>
                            @endisset
                            @isset($team->twitter)
                                <a title="Twitter" target="_blank" href="{{$team->twitter}}"><i class="fab fa-twitter"></i><span> </span></a>
                            @endisset
                            @isset($team->linkedin)
                                <a title="Linkedin" target="_blank" href="{{$team->linkedin}}"><i class="fab fa-linkedin-in"></i><span> </span></a>
                            @endisset
                            @isset($team->youtube)
                                <a title="Youtube" target="_blank" href="{{$team->youtube}}"><i class="fab fa-youtube"></i><span> </span></a>
                            @endisset
                        </span>
                    </span>
                </span>
                </div>
            @endforeach
        </div>
        </div>
    </div>
@endsection
