@extends('layouts.site')
@section('title')
    {{__('app.main_page')}}
@endsection
@section('content')
    <?php
    use App\Models\Category;
    $projects=\App\Models\Project::orderby('id','desc')->take(6)->get();
    $categories= array();
    foreach ($projects as $cat ){
        if($cat->Category){
            if(in_array($cat->Category,$categories))
            continue;
            else
            $arr= array_push($categories ,$cat->Category);
        }
    }
//    $category =Category::with('Project')->where('type', '1')->orderBy('id','DESC')->get();
//    $categories = \App\Models\Category::withCount('products')->latest('products_count')->take(10)->with('products')->get();
//    $categories = \App\Models\Category::with('Project')->whereHas('Project', function($q){
//        $q->where('id','=','1')->take(3);
//    })->get();
    $services=\App\Models\Service::orderby('id','desc')->take(6)->get();
    $teams=\App\Models\Team::orderby('id','desc')->take(3)->get();
    $sliders=\App\Models\Slider::all();
    ?>
<!-- Begin SideBar-->
<div role="main" class="main" id="home" >
    <div id="slider">
    <div class="row">
        <div class="col">
            <div class="owl-carousel nav-inside show-nav-hover  mb-0" data-plugin-options="{'items': 1, 'loop': true, 'autoplay': true, 'autoplayTimeout': 9000, 'autoplayHoverPause': true, 'nav': true, 'dots': false, 'animateOut': 'fadeOut'}">
                @foreach($sliders as $slider)
                <div class="slider" style="background-image: url({{asset("admin/{$slider->image}")}}); background-size: cover; background-position: center; height: 34rem">
                    <div class="container position-relative z-index-3 h-100">
                        <div class="row justify-content-center align-items-center h-100">
                            <div class="col-lg-6">
                                <div class="d-flex flex-column align-items-center">
                                    <h2 class="position-relative text-color-light font-weight-extra-bold text-12 px-4 mb-4 appear-animation" data-appear-animation="blurIn" data-appear-animation-delay="800" data-plugin-options="{'firstLoadNoAnim': true, 'minWindowWidth': 0}">
                                        <span class="position-absolute right-100pct top-50pct transform3dy-n50 opacity-3">
                                                <img src="{{asset('front/img/sliders/slide-title-border.png')}}" class="w-auto appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="250" data-plugin-options="{'firstLoadNoAnim': true, 'minWindowWidth': 0}" alt="" />
                                            </span>
                                        {{$slider->title}}
                                        <span class="position-absolute left-100pct top-50pct transform3dy-n50 opacity-3">
                                                <img src="{{asset('front/img/sliders/slide-title-border.png')}}" class="w-auto appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="250" data-plugin-options="{'firstLoadNoAnim': true, 'minWindowWidth': 0}" alt="" />
                                            </span>
                                    </h2>
                                    <p class="text-4 text-color-light font-weight-light opacity-7 text-center mt-2 mb-4" data-plugin-animated-letters data-plugin-options="{'startDelay': 2000, 'firstLoadNoAnim': true, 'minWindowWidth': 0, 'animationSpeed': 20}"> <strong class="text-color-light">{{$slider->text}}</strong> </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    </div>

    <div id="projects" class="container ">
        <div class="row justify-content-center pt-5 mt-5">
            <div id="project"  class="col-lg-9 text-center">
                <div class="appear-animation" data-appear-animation="fadeInUpShorter">
                    <h2 class="font-weight-bold mb-2">{{__('app.project')}}</h2>
                    <p class="mb-4">{{__('app.We care about providing the best at the highest level and in the fastest time')}}</p>
                </div>
            </div>
        </div>
{{--        <div class="row mb-5 justify-content-center appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">--}}
{{--            <div class="col">--}}

{{--                <div class="appear-animation popup-gallery-ajax" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">--}}
{{--                    <div class="owl-carousel owl-theme mb-0" data-plugin-options="{'items': 4, 'margin': 35, 'loop': false}">--}}
{{--                     @foreach($projects as $project)--}}
{{--                            <div class="portfolio-item">--}}
{{--                                <a href="{{url("project/$project->id")}}" data-ajax-on-modal>--}}
{{--                                    <span class="thumb-info thumb-info-lighten">--}}
{{--                                        <span class="thumb-info-wrapper">--}}
{{--                                                <img @if($project->image != null) src="{{asset("admin/{$project->image}")}}"  @else  src="{{asset("front/img/THE_WEBDIZER_LOGO-04.png")}}" @endif height="220px" style=" background: #778cbf;" alt="">--}}
{{--                                                <span class="thumb-info-title">--}}
{{--                                                    <span class="thumb-info-inner">{{$project->title}}</span>--}}
{{--                                                    <span class="thumb-info-type">{{$project->Category->name}}</span>--}}
{{--                                                </span>--}}
{{--                                            </span>--}}
{{--                                            <span class="thumb-info-action">--}}
{{--                                                <span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>--}}
{{--                                            </span>--}}
{{--                                        </span>--}}
{{--                                    </span>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                    @endforeach--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="py-4 bg-light left-0  w-100" >
                    <div id="findBtn" class="text-center appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="400" data-appear-animation-duration="750">

                        <ul id="catID" class="nav nav-pills sort-source sort-source-style-3 justify-content-center mb-0"
                            data-sort-id="portfolio" data-option-key="filter"
                            data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*', 'useHash': false}">

                            @foreach($categories as $category)
    {{--                            @if(count($category->Project))--}}

                                    <li id="{{$category->id}}" class="nav-item " data-option-value="{{$category->id}}">
                                        <a id="{{$category->id}}" class="nav-link custom-nav-link " href="#demos" data-hash=""
                                           data-hash-offset="-200">{{$category->name}}</a>
                                    </li>
    {{--                            @endif--}}
                            @endforeach
                            <li id="all" class="nav-item ">
                                <a class="nav-link custom-nav-link " href="">الكل</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id="productData">
                    <div class="row justify-content-center portfolio-list sort-destination overflow-visible mt-4"
                         data-sort-id="portfolio" data-filter="{{$category->id}}" style="position: relative; height: 3987.3px !important;">

                    @foreach($projects as $project)
                            <div class="col-sm-6 col-md-4 col-lg-4 isotope-item appear-animation  " style="left: 0px; top: 0px;"  data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300">

                                <div class="portfolio-item">
                                <a href="{{url("project/$project->id")}}" data-ajax-on-modal>
                                    <span class="thumb-info thumb-info-lighten">
                                        <span class="thumb-info-wrapper">
                                            <img @if($project->image != null) src="{{asset("admin/{$project->image}")}}"
                                                 @else src="{{asset("front/img/THE_WEBDIZER_LOGO-04.png")}}" @endif
                                                 height="220px" style=" background: #778cbf;" alt="">
                                            <span class="thumb-info-title">
                                                <span class="thumb-info-inner">{{$project->title}}</span>
                                                <span class="thumb-info-type">{{$project->Category->name}}</span>
                                            </span>
                                            <span class="thumb-info-action">
                                                <span class="thumb-info-action-icon bg-dark opacity-8">
                                                    <i class="fas fa-plus"></i></span>
                                            </span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
                </div>
            <div class="pb-5" style="text-align: center">
            <a href="{{url('project')}}" class="btn p-relative top-5 pad-a" style="color: #fff !important;letter-spacing: 1.1px;background-color: #0088cc;">
                <strong class="text-2">{{__('app.VIEW MORE_project')}}</strong>
                <i class="fas fa-plus p-relative top-1 pl-2"></i>
            </a>
        </div>
        </div>
{{--    </div>--}}
<div @if(LaravelLocalization::getCurrentLocale()=='ar') dir="rtl" style="text-align: right" @else dir="ltr" @endif>
    <section id="services" class="section section-height-3 bg-primary border-0 m-0 appear-animation" data-appear-animation="fadeIn">
        <div class="container my-3">
            <div class="row mb-5">
                <div class="col text-center appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">
                    <h2 class="font-weight-bold text-color-light mb-2">{{__('app.services')}}</h2>
                    <p class="text-color-light opacity-7">{{__('app.we are here to help you')}}</p>
                </div>
            </div>

            <div class="row mb-4 justify-content-center">
{{--                <div class="card-group ">--}}
                    @foreach($services as $service)
                    <div class="col-sm-6 col-md-4 col-lg-4 col-xl-1-4 appear-animation text-center pt-5 "  data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="300">
                       <div class="bg-white rounded p-3 shadow-sm">
                        <div class="feature-box feature-box-style-2">
                            <div class="feature-box-icon">
                            <i class="icons icon-layers " style="color: #08c !important;"></i>
                            </div>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{$service->title}}</h5>
                            <p class="card-text">{!!  \Illuminate\Support\Str::limit( $service->text, 150) !!}</p>
{{--                            <p class="card-text">--}}

{{--                                    <a href="{{url("service/$service->id")}}" style="color: #08c !important;" class="btn-flat btn-xs text-color-light p-relative top-5">--}}
{{--                                        <strong class="text-2">{{__('app.VIEW MORE')}}</strong>--}}
{{--                                        <i class="fas fa-plus p-relative top-1 pl-2"></i>--}}
{{--                                    </a>--}}

{{--                            </p>--}}
                        </div>
                    </div>
                    </div>
                    @endforeach

{{--                </div>--}}

{{--                @foreach($services as $service)--}}
{{--                <div class="col-lg-4 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="300">--}}
{{--                    <div class="feature-box feature-box-style-2">--}}
{{--                        <div class="feature-box-icon">--}}
{{--                            <i class="icons icon-layers text-color-light"></i>--}}
{{--                        </div>--}}
{{--                        <div class="feature-box-info">--}}
{{--                            <h4 class="font-weight-bold text-color-light text-4 mb-2">{{$service->name}}</h4>--}}
{{--                            <p class="text-color-light opacity-7">{!!  \Illuminate\Support\Str::limit( $service->text, 150) !!}</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                @endforeach--}}
            </div>
            <div style="text-align: center ; padding-top: 1rem;">
                <a href="{{url('service')}}" class="btn btn-light p-relative top-5 pad-a" style="color: #08c !important;letter-spacing: 1.1px;">
                    <strong class="text-2">{{__('app.VIEW MORE_service')}}</strong>
                    <i class="fas fa-plus p-relative top-1 pl-2"></i>
                </a>
            </div>
        </div>
    </section>

    <div id="team" class="container pb-4">
        <div class="row pt-5 mt-5 mb-4">
            <div class="col text-center appear-animation" data-appear-animation="fadeInUpShorter">
                <h2 class="font-weight-bold mb-2">{{__('app.Team')}}</h2>
                <p>{{__('app.our_people')}} </p>
            </div>
        </div>
        <div class="row mb-5 justify-content-center appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">
            @foreach($teams as $team)
            <div class="col-sm-6 col-lg-4 mb-4 mb-lg-0">
                <span class="thumb-info thumb-info-hide-wrapper-bg thumb-info-no-zoom">
                    <span class="thumb-info-wrapper">
                        <a href="{{url('team')}}">
                            <img class="img-fluid"  @if($team->image != null)src="{{asset("admin/{$team->image}")}}" @else
                            src="{{asset("front/img/default.jpg")}}" @endif alt=""/>
                        </a>
                    </span>
                    <span class="thumb-info-caption">
                        <h3 class="font-weight-extra-bold text-color-dark text-4 line-height-1 mt-3 mb-0">{{$team->name}}</h3>
                        <span class="text-2 mb-0">{{$team->job}}</span>
                        <span class="thumb-info-caption-text pt-1">
                            {!!  \Illuminate\Support\Str::limit( $team->text, 100) !!}
                            </span>
                        <span class="thumb-info-social-icons">
                            @isset($team->facebook)
                            <a title="Facebook" target="_blank" href="{{$team->facebook}}"><i class="fab fa-facebook-f"></i><span> </span></a>
                            @endisset
                            @isset($team->twitter)
                            <a title="Twitter" target="_blank" href="{{$team->twitter}}"><i class="fab fa-twitter"></i><span> </span></a>
                            @endisset
                            @isset($team->linkedin)
                            <a title="Linkedin" target="_blank" href="{{$team->linkedin}}"><i class="fab fa-linkedin-in"></i><span> </span></a>
                            @endisset
                            @isset($team->youtube)
                            <a title="Youtube" target="_blank" href="{{$team->youtube}}"><i class="fab fa-youtube"></i><span> </span></a>
                            @endisset
                        </span>
                    </span>
                </span>
            </div>
            @endforeach
        </div>
        <div class="pb-5" style="text-align: center">
            <a href="{{url('team')}}" class="btn p-relative top-5 pad-a" style="color: #fff !important;letter-spacing: 1.1px;background-color: #0088cc;">
                <strong class="text-2">{{__('app.VIEW MORE_team')}}</strong>
                <i class="fas fa-plus p-relative top-1 pl-2"></i>
            </a>
        </div>
    </div>


</div>
<!--End Sidebare-->
@endsection
@section('script')
    <script>

        document.getElementById("catID").addEventListener("click",function(e) {
            if(e.target && e.target.nodeName == "A") {
                var cat = e.target.id; console.log(e.target.id);
                $.ajax({
                    type: 'get',
                    dataType: 'html',
                    url: '{{url('productsCat')}}',
                    data: 'cat_id=' + cat ,
                    success:function(response){
                        // console.log(response);
                        $("#productData").html(response);
                    }
                });
            }

        });

    </script>
{{--    <script>--}}
{{--        $(document).ready(function () {--}}
{{--            $('.nav-item').click(function () {--}}
{{--                var category = $(this).attr('id');--}}
{{--                if (category == 'all'){--}}
{{--                    $('.isotope-item ').addClass('hide');--}}
{{--                    setTimeout(function () {--}}
{{--                        $('.isotope-item ').removeClass('hide');--}}
{{--                    },300);--}}
{{--                }--}}
{{--            })--}}

{{--        })--}}
{{--    </script>--}}
@endsection
