@extends('layouts.site')

@section('title')
    {{__('app.project')}}
@endsection
@section('content')
{{--    <div role="main" class="main" @if(LaravelLocalization::getCurrentLocale()=='ar') dir="rtl" style="text-align: right"  @endif>--}}
        <section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7" style="background-image: url({{asset('front/img/photos.jpg')}}); background-position-y: center;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 align-self-center p-static order-2 text-center">
                        <h1 class="text-10">{{__('app.project')}}</h1>
                        <span class="sub-title pt-2">{{__('app.we are here to help you')}}</span>
                    </div>
                </div>
            </div>
        </section>

{{--        <div class="container py-4">--}}
{{--            <div class="row py-3 justify-content-center">--}}
{{--                <div class="col-lg-3">--}}
{{--                    <aside class="sidebar">--}}
{{--                        <h5 class="font-weight-semi-bold pt-4">{{__('app.Categories')}}</h5>--}}
{{--                        <ul class="nav nav-list flex-column mb-5">--}}
{{--                            @foreach($categories as $category)--}}
{{--                                <li class="nav-item list">--}}
{{--                                    <a class="nav-link active" href="#">{{$category->name}}</a>--}}
{{--                                    <ul>--}}
{{--                                        @foreach($projects as $project)--}}
{{--                                            @if($project ->CategoryID == $category->id)--}}
{{--                                                <li class="nav-item"><a href="{{url("project/$project->id")}}" data-ajax-on-modal>{{$project->title}}</a></li>--}}
{{--                                            @endif--}}
{{--                                        @endforeach--}}

{{--                                    </ul>--}}
{{--                                </li>--}}
{{--                            @endforeach--}}
{{--                        </ul>--}}
{{--                    </aside>--}}
{{--                </div>--}}
{{--                <div class="col-lg-9">--}}
{{--                <div class="row justify-content-center ">--}}
{{--               @foreach($projects as $project)--}}

{{--                <div class="col-sm-8 col-md-4 col-lg-4 mb-4 appear-animation" data-appear-animation="fadeIn">--}}

{{--                                <div class="portfolio-item">--}}
{{--                                    <a href="{{url("project/$project->id")}}" data-ajax-on-modal>--}}
{{--											<span class="thumb-info thumb-info-lighten">--}}
{{--												<span class="thumb-info-wrapper">--}}
{{--													<img @if($project->image != null) src="{{asset("admin/{$project->image}")}}"  @else src="{{asset("front/img/THE_WEBDIZER_LOGO-04.png")}}" @endif height="220px" style=" background: #778cbf;" alt="">--}}
{{--													<span class="thumb-info-title">--}}
{{--														<span class="thumb-info-inner">{{$project->title}}</span>--}}
{{--														<span class="thumb-info-type">{{$project->Category->name}}</span>--}}
{{--													</span>--}}
{{--													<span class="thumb-info-action">--}}
{{--														<span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>--}}
{{--													</span>--}}
{{--												</span>--}}
{{--											</span>--}}
{{--                                    </a>--}}
{{--                                </div>--}}
{{--                </div>--}}

{{--                @endforeach--}}
{{--              </div>--}}
{{--            </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <section id="demos" class="section section-no-border section-light position-relative pt-0 m-0">

        <div class="container">
{{--            <div class="pin-wrapper" style="height: 84px;">--}}
                <div class="py-4 bg-light left-0  w-100" >
                    <div id="findBtn" class="text-center appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="400" data-appear-animation-duration="750">

                        <ul id="catID" class="nav nav-pills sort-source sort-source-style-3 justify-content-center mb-0"
                            data-sort-id="portfolio" data-option-key="filter"
                            data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*', 'useHash': false}">
                            @foreach($categories as $category)
                                @if(count($category->Project))
                                <li id="{{$category->id}}" class="nav-item " data-option-value="{{$category->id}}">
                                    <a id="{{$category->id}}" class="nav-link custom-nav-link " href="#demos" data-hash=""
                                       data-hash-offset="-200">{{$category->name}}</a>
                                </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            <div id="productData">
            <div class="row justify-content-center portfolio-list sort-destination overflow-visible mt-4"
                 data-sort-id="portfolio" data-filter="{{$category->id}}" style="position: relative; height: 3987.3px !important;">

                @foreach($projects as $project)
                        <div class="col-sm-6 col-md-4 col-lg-3 isotope-item " style="left: 0px; top: 0px;" data-appear-animation="fadeIn">

                            <div class="portfolio-item">
                                <a href="{{url("project/$project->id")}}" data-ajax-on-modal>
                                        <span class="thumb-info thumb-info-lighten">
                                            <span class="thumb-info-wrapper">
                                                <img @if($project->image != null) src="{{asset("admin/{$project->image}")}}"
                                                     @else src="{{asset("front/img/THE_WEBDIZER_LOGO-04.png")}}" @endif
                                                     height="220px" style=" background: #778cbf;" alt="">
                                                <span class="thumb-info-title">
                                                    <span class="thumb-info-inner">{{$project->title}}</span>
                                                    <span class="thumb-info-type">{{$project->Category->name}}</span>
                                                </span>
                                                <span class="thumb-info-action">
                                                    <span class="thumb-info-action-icon bg-dark opacity-8">
                                                        <i class="fas fa-plus"></i></span>
                                                </span>
                                            </span>
                                        </span>
                                </a>
                            </div>
                        </div>
                @endforeach

                </div>
            </div>
            {{$projects->links()}}
        </div>
        </section>
{{--    </div>--}}
@endsection
@section('script')
    <script>

        document.getElementById("catID").addEventListener("click",function(e) {
                if(e.target && e.target.nodeName == "A") {
                    var cat = e.target.id;
                    $.ajax({
                        type: 'get',
                        dataType: 'html',
                        url: '{{url('productsCat')}}',
                        data: 'cat_id=' + cat ,
                        success:function(response){
                            // console.log(response);
                            $("#productData").html(response);
                        }
                    });
                }

        });

        $(document).on('click', ' nav > ul > li' , function () {
            $this.addClass('active ').siblings().removeClass('active ')
        });
    </script>
@endsection
