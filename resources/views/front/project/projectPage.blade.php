<!DOCTYPE html>
<html>
<head>

    <!-- Skin CSS -->
    <link id="skinCSS" rel="stylesheet" href="{{asset('front/css/skins/default.css')}}">

</head>
<body class="one-page alternative-font-5" data-target="#header" data-spy="scroll" data-offset="100">
    <div class="body">

    <div class="row justify-content-center portfolio-list overflow-visible mt-4">

    @foreach($projects as $project)
        <div class="col-sm-6 col-md-4 col-lg-3 col-xl-1-4 isotope-item classic" style="left: 0px; top: 0px;" >
            <div class="portfolio-item">
                <a href="{{url("project/$project->id")}}" data-ajax-on-modal>
                                            <span class="thumb-info thumb-info-lighten">
                                                <span class="thumb-info-wrapper">
                                                    <img @if($project->image != null) src="{{asset("admin/{$project->image}")}}"
                                                         @else src="{{asset("front/img/THE_WEBDIZER_LOGO-04.png")}}" @endif
                                                         height="220px" style=" background: #778cbf;" alt="">
                                                    <span class="thumb-info-title">
                                                        <span class="thumb-info-inner">{{$project->title}}</span>
                                                        <span class="thumb-info-type">{{$project->Category->name}}</span>
                                                    </span>
                                                    <span class="thumb-info-action">
                                                        <span class="thumb-info-action-icon bg-dark opacity-8">
                                                            <i class="fas fa-plus"></i></span>
                                                    </span>
                                                </span>
                                            </span>
                </a>
            </div>
        </div>
    @endforeach
    </div>
    </div>

</body>

<!-- Examples -->
<script src="{{asset('front/js/examples/examples.portfolio.js')}}"></script>
