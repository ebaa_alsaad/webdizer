@extends('layouts.site')

@section('title')
  {{__('app.about')}}
@endsection
@section('content')
    <?php
    $teams=\App\Models\Team::get();
    $userExperiences = \App\Models\UserExperience::orderBy('id','DESC')->get();
    $partners = \App\Models\Partner::orderBy('id','DESC')->get();
    ?>
<div id="about">
<section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7" style="background-image: url({{asset('front/img/page-header-about-us.jpg')}});">
    <div class="container">
        <div class="row">
            <div class="col-md-12 align-self-center p-static order-2 text-center">
                <h1 class="text-9 font-weight-bold">{{__('app.about')}}</h1>
                <span class="sub-title pt-2">{{__('app.The perfect choice')}}</span>
            </div>
        </div>
    </div>
</section>
<section class=" section-height-3 py-5 m-0 border-0" @if(LaravelLocalization::getCurrentLocale()=='ar') dir="rtl" style="text-align: right" @else dir="ltr" @endif>
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-lg-6 pb-sm-4 pb-lg-0 pr-lg-5 mb-sm-5 mb-lg-0">
                <h2 class="text-color-dark font-weight-normal text-6 mb-2">{{__('app.Who')}} <strong class="font-weight-extra-bold">
                    {{__('app.We Are')}} </strong></h2>
                <p class=" mr-5" style="letter-spacing: 0.7px;">{{$about->value}}</p>
            </div>
            <div id="about_img" class="col-sm-8 col-md-6 col-lg-4 offset-sm-4 offset-md-4 offset-lg-2 mt-sm-5" style="top: 1.7rem;">
                <img src="{{asset('front/img/generic-corporate-3-2.jpg')}}" class="img-fluid position-absolute d-none d-sm-block appear-animation" data-appear-animation="expandIn" data-appear-animation-delay="300" style="top: 10%; left: -50%;" alt="" />
                <img src="{{asset('front/img/about.jpg')}}" width="80%" class="img-fluid position-absolute d-none d-sm-block appear-animation" data-appear-animation="expandIn" style="top: -33%; left: -29%;" alt="" />
                <img src="{{asset('front/img/generic-corporate-3-3.jpg')}}" class="img-fluid position-relative appear-animation mb-2" data-appear-animation="expandIn" data-appear-animation-delay="600" alt="" />
            </div>
        </div>
    </div>
</section>

    <div class="container my-5 pt-5">
        <h2 class="font-weight-normal text-6 mb-4 text-center"><strong class="font-weight-extra-bold">{{__('app.What Client’s Say')}}</h2>
        <div class="owl-carousel owl-theme mb-0" data-plugin-options="{'items': 1, 'autoHeight': true, 'autoplay': true, 'autoplayTimeout': 8000}">
            @foreach($userExperiences as $userExperience)
            <div class="row">

                        <div class="col"  @if(LaravelLocalization::getCurrentLocale()=='ar') dir="rtl" style="text-align: right" @else dir="ltr" @endif>

                            <div class="testimonial testimonial-primary">
                                <blockquote>
                                    <p class="mb-0">{{$userExperience->text}}</p>
                                </blockquote>
                                <div class="testimonial-arrow-down"></div>
                                <div class="testimonial-author">
                                    <p><strong>{{$userExperience->name}}</strong></p>
                                </div>
                            </div>

                        </div>


            </div>
            @endforeach
        </div>
    </div>

    <div class="container my-5 pt-5">
        <h2 class="font-weight-normal text-6 mb-4 text-center"><strong class="font-weight-extra-bold">{{__('app.Our partners')}}</h2>
        <div class="owl-carousel owl-theme  mb-0" data-plugin-options="{'items': 3, 'margin': 35,  'autoHeight': true, 'autoplay': true, 'autoplayTimeout': 8000}">
                @foreach($partners as $partner)
<<<<<<< HEAD
{{--                <div class="row p-5 mb-5 justify-content-center appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">--}}
{{--                    <div class="col-sm-6 col-lg-4 mb-4 mb-lg-0">--}}
                    <span class="thumb-info thumb-info-hide-wrapper-bg thumb-info-no-zoom">
                        <span class="thumb-info-wrapper">
                            <a target="_blank" href="{{$partner->link}}">
                                <img class="img-fluid"  @if($partner->image != null)src="{{asset("admin/{$partner->image}")}}" @else
                                src="{{asset("front/img/default.jpg")}}" @endif alt=""/>
                            </a>
                        </span>
=======
                    <div class="col-sm-6 col-lg-4 mb-4 mb-lg-0">
                <span class="thumb-info thumb-info-hide-wrapper-bg thumb-info-no-zoom">
                    <span class="thumb-info-wrapper">
                        <a target="_blank" href="{{$partner->link}}">
                            <img class="img-fluid"  @if($partner->image != null)src="{{asset("admin/{$partner->image}")}}" @else
                            src="{{asset("front/img/default.jpg")}}" @endif alt=""/>
                        </a>
>>>>>>> 0fc7edb0075c8fd264fb6cc8b9a699200f57c673
                    </span>
{{--                    </div>--}}
{{--                </div>--}}
                @endforeach
        </div>
    </div>
</div>
@endsection

