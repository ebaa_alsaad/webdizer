
    <div class="ajax-container detail">
        <div class="row align-items-center py-4">

            <div class="col">
                <a href="#" data-ajax-portfolio-prev class="text-decoration-none d-block float-left">
                    <div class="d-flex align-items-center line-height-1">
                        <i class="fas fa-arrow-left text-dark text-4 mr-3"></i>
                        <div class="d-none d-sm-block line-height-1">
                            <span class="text-dark opacity-4 text-1">{{__('app.PREVIOUS news')}}</span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col">
                <div class="row">
                    <div class="col-md-12 align-self-center p-static order-2 text-center">
                        <div class="overflow-hidden pb-2">
                            <h2 class="text-dark font-weight-bold text-7 mb-0">{{$news->title}}</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <a href="#" data-ajax-portfolio-next class="text-decoration-none d-block float-right">
                    <div class="d-flex align-items-center text-right line-height-1">
                        <div class="d-none d-sm-block line-height-1">
                            <span class="text-dark opacity-4 text-1">{{__('app.NEXT news')}}</span>
                        </div>
                        <i class="fas fa-arrow-right text-dark text-4 ml-3"></i>
                    </div>
                </a>
            </div>

        </div>

        <div class="row pb-4 my-2">
            <div class="col-sm-6 mt-2">

                <div class="img-thumbnail border-0 border-radius-0 p-0 d-block">
                    <img @if($news->image != null) src="{{asset("admin/{$news->image}")}}"  @else  src="{{asset("front/img/THE_WEBDIZER_LOGO-04.png")}}" @endif height="220px" style=" background: #778cbf;" class="img-fluid"  alt="">
                </div>

            </div>

                @if(LaravelLocalization::getCurrentLocale()=='en')
                <div class="col-sm-6">
                    <h2 class="text-color-dark font-weight-normal text-4 mb-0">
                        {{__('app.newss')}}
                        <strong class="font-weight-extra-bold">
                        {{__('app.Description')}}</strong>
                    </h2>
                    <hr class="solid my-2">

                    <p>{{$news->text}}</p>
                </div>
                @else
                <div class="col-sm-6"style="text-align: right">
                    <h2 class="text-color-dark font-weight-normal text-4 mb-0" style="text-align: right">
                        {{__('app.Description')}}</strong>
                        <strong class="font-weight-extra-bold">
                        {{__('app.newss')}}
                    </h2>
                    <hr class="solid my-2">

                    <p style="font-weight: 500;">{{$news->text}}</p>

            </div>
            @endif

        </div>
    </div>
