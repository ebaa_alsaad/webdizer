﻿@extends('layouts.admin')
@section('title')
    | إضافة خبر جديد
@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('Admin/dashboard')}}">الرئيسية </a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{url('Admin/news')}}"> الأخبار </a>
                                </li>
                                <li class="breadcrumb-item active">إضافة خبر جديد
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i
                                            class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                   @include('admin.includes.alerts.success')
                                   @include('admin.includes.alerts.errors')
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form" action="{{url('Admin/news')}}" method="POST"
                                              enctype="multipart/form-data">
                                              @csrf
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-home"></i> بيانات الخبر </h4>

                                                <div class="row">
                                                    <div class="col-md-6" >
                                                        <div class="form-group">
                                                            <label for="CategoryID"> التصنيف </label>
                                                            <select class="form-control" name="CategoryID" id="CategoryID">
                                                                @foreach ($category as $key => $cat)
                                                                    @if($category[ $key++ ]->type == 2)
                                                                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                            @error("CategoryID")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="title"> الاسم AR</label>
                                                            <input type="text"
                                                                   value="{{ old('title') }}"
                                                                   required autocomplete="title"
                                                                   id="title"
                                                                   class="form-control"
                                                                   placeholder="ادخل اسم المشروع "
                                                                   name="title:ar">
                                                            @error("title:ar")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="title"> الاسم EN </label>
                                                            <input type="text"
                                                                   value="{{ old('title') }}"
                                                                   required autocomplete="title"
                                                                   id="title"
                                                                   class="form-control"
                                                                   placeholder="ادخل اسم المشروع "
                                                                   name="title:en">
                                                            @error("title:en")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label> صورة الخدمة </label>
                                                            <label id="image" class="file center-block">
                                                                <input type="file" id="image" name="image">
                                                                <span class="file-custom"></span>
                                                            </label>
                                                            @error("image")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12" >
                                                        <div class="form-group">
                                                            <label for="text"> الوصف AR</label>
                                                            <textarea type="text"
                                                                   value="{{ old('text') }}"
                                                                   id="text"
                                                                   rows="6"
                                                                   class="form-control"
                                                                   placeholder="ادخل الوصف "
                                                                   name="text:ar"
                                                                   required >
                                                            </textarea>
                                                            @error("text:ar")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12" >
                                                        <div class="form-group">
                                                            <label for="text"> الوصف EN </label>
                                                            <textarea type="text"
                                                                      value="{{ old('text') }}"
                                                                      id="text"
                                                                      rows="6"
                                                                      style="direction: ltr;"
                                                                      class="form-control"
                                                                      placeholder="ادخل الوصف "
                                                                      name="text:en"
                                                                      required >
                                                            </textarea>
                                                            @error("text:en")
                                                            <span class="text-danger">{{$message}} </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group mt-1">
                                                            <label for="status"
                                                                   class="card-title ml-1"> الحالة </label>

                                                            <input type="checkbox" value="1" name="status"
                                                                   id="status"
                                                                   class="switchery" data-color="success"
                                                                   checked/>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="form-actions">
                                                <button type="button" class="btn btn-outline-warning box-shadow-1 mr-1"
                                                        onclick="history.back();">
                                                    <i class="ft-x"></i> تراجع
                                                </button>
                                                <button type="submit" class="btn btn-outline-primary box-shadow-1">
                                                    <i class="la la-check-square-o"></i> حفظ
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
@endsection
