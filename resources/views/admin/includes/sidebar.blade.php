<!-- Begin SideBar-->
<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            @can('المستخدمين')
            <li class="nav-item"><a href=""><i class="la la-group"></i>
                    <span class="menu-title" data-i18n="nav.dash.main">المستخدمين </span>
                    <span
                        class="badge badge badge-danger badge-pill float-right mr-2">{{App\Models\User::count()}}</span>
                </a>
                <ul class="menu-content">
                    @can('قائمة المستخدمين')
                    <li><a class="menu-item" href="{{url('Admin/users')}}"
                                          data-i18n="nav.dash.ecommerce"> قائمة المستخدمين </a>
                    </li>
                    @endcan
                    @can('إضافة مستخدم جديد')
                    <li><a class="menu-item" href="{{url('Admin/users/create')}}" data-i18n="nav.dash.crypto">
                            إضافة مستخدم جديد</a>
                    </li>
                    @endcan
                    @can('صلاحيات المستخدمين')
                    <li><a class="menu-item" href="{{url('Admin/role')}}" data-i18n="nav.dash.crypto">صلاحيات المستخدمين </a>
                    </li>
                    @endcan
                    @can('إضافة مجموعة صلاحيات')
                    <li><a class="menu-item" href="{{url('Admin/role/create')}}" data-i18n="nav.dash.crypto">إضافة مجموعة صلاحيات </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('الخدمات')
            <li class="nav-item"><a href=""> <i class="la la-suitcase"></i>

                    <span class="menu-title" data-i18n="nav.dash.main"> الخدمات </span>
                    <span
                        class="badge badge badge-info badge-pill float-right mr-2">{{App\Models\Service::count()}}</span>
                </a>
                <ul class="menu-content">
                    @can('عرض الخدمات')
                    <li><a class="menu-item" href="{{url('Admin/service')}}"
                           data-i18n="nav.dash.ecommerce"> عرض الخدمات </a>
                    </li>
                    @endcan
                    @can( 'إضافة خدمة جديدة')
                    <li><a class="menu-item" href="{{url('Admin/service/create')}}" data-i18n="nav.dash.crypto">
                            إضافة خدمة جديدة </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('المشاريع')
            <li class="nav-item"><a href=""><i class="la la-wrench"></i>
                    <span class="menu-title" data-i18n="nav.dash.main">المشاريع </span>
                    <span
                        class="badge badge badge-info badge-pill float-right mr-2">{{App\Models\Project::count()}}</span>
                </a>
                <ul class="menu-content">
                    @can(' عرض المشاريع')
                    <li><a class="menu-item" href="{{url('Admin/project')}}"
                           data-i18n="nav.dash.ecommerce"> عرض المشاريع </a>
                    </li>
                    @endcan
                    @can(' إضافة مشروع جديد')
                    <li><a class="menu-item" href="{{url('Admin/project/create')}}" data-i18n="nav.dash.crypto">
                            إضافة مشروع جديد </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('الأخبار')
            <li class="nav-item"><a href=""><i class="la la-newspaper-o"></i>
                    <span class="menu-title" data-i18n="nav.dash.main">الأخبار </span>
                    <span
                        class="badge badge badge-info badge-pill float-right mr-2">{{App\Models\News::count()}}</span>
                </a>
                <ul class="menu-content">
                    @can(' عرض الأخبار')
                    <li><a class="menu-item" href="{{url('Admin/news')}}"
                           data-i18n="nav.dash.ecommerce"> عرض الأخبار </a>
                    </li>
                    @endcan
                    @can('إضافة خبر جديد')
                    <li><a class="menu-item" href="{{url('Admin/news/create')}}" data-i18n="nav.dash.crypto">
                            إضافة خبر جديد </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('فريق العمل')
            <li class="nav-item"><a href=""><i class="la la-user"></i>
                    <span class="menu-title" data-i18n="nav.dash.main">فريق العمل </span>
                    <span
                        class="badge badge badge-info badge-pill float-right mr-2">{{App\Models\Team::count()}}</span>
                </a>
                <ul class="menu-content">
                    @can('عرض الأعضاء')
                    <li><a class="menu-item" href="{{url('Admin/team')}}"
                           data-i18n="nav.dash.ecommerce"> عرض الأعضاء</a>
                    </li>
                    @endcan
                    @can('إضافة جديد')
                    <li><a class="menu-item" href="{{url('Admin/team/create')}}" data-i18n="nav.dash.crypto">
                            إضافة جديد </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('التصنيفات')
            <li class="nav-item"><a href=""><i class="la la-paperclip"></i>
                    <span class="menu-title" data-i18n="nav.dash.main">التصنيفات </span>
                </a>
                <ul class="menu-content">
                    @can('تصنيف المشاريع')
                    <li class="nav-item"><a href="">
                            <span class="menu-title" data-i18n="nav.dash.main"> تصنيف المشاريع </span>
                            <span class="badge badge badge-success badge-pill float-right mr-2">{{App\Models\Category::where('type' , 1)->count()}}</span>

                        </a>
                        <ul class="menu-content">
                            @can('عرض تصنيفات المشاريع')
                            <li>
                                <a lass="menu-item" href="{{url('/Admin/category?type=1')}}"data-i18n="nav.dash.ecommerce"> عرض تصنيفات المشاريع</a>
                            </li>
                            @endcan
                            @can('إضافة تصنيف مشروع')
                            <li>
                                <a lass="menu-item" href="{{url('/Admin/category/create?type=1')}}"data-i18n="nav.dash.ecommerce">إضافة تصنيف مشروع</a>

                            </li>
                            @endcan
                        </ul>
                    </li>
                    @endcan
                    @can('تصنيف الأخبار')
                    <li class="nav-item"><a href="">
                            <span class="menu-title" data-i18n="nav.dash.main"> تصنيف الأخبار </span>
                            <span class="badge badge badge-success badge-pill float-right mr-2">{{App\Models\Category::where('type' , 2)->count()}}</span>

                        </a>
                        <ul class="menu-content">
                            @can('عرض تصنيفات الأخبار')
                            <li>
                                <a lass="menu-item" href="{{url('/Admin/category?type=2')}}"data-i18n="nav.dash.ecommerce">عرض تصنيفات الأخبار </a>
                            </li>
                            @endcan
                            @can('إضافة تصنيف خبر')
                            <li>
                                <a lass="menu-item" href="{{url('/Admin/category/create?type=2')}}"data-i18n="nav.dash.ecommerce">إضافة تصنيف خبر</a>
                            </li>
                            @endcan
                        </ul>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('سلايدر الصفحة الرئيسية')
            <li class="nav-item"><a href=""><i class="la la-dedent"></i>
                    <span class="menu-title" data-i18n="nav.dash.main">سلايدر الصفحة الرئيسية </span>
                </a>
                <ul class="menu-content">
                    @can(' عرض Slider')
                    <li><a class="menu-item" href="{{url('Admin/slider')}}"
                           data-i18n="nav.dash.ecommerce"> عرض Slider </a>
                    </li>
                    @endcan
                    @can('إضافة Slider جديد')
                    <li><a class="menu-item" href="{{url('Admin/slider/create')}}" data-i18n="nav.dash.crypto">
                            إضافة Slider جديد </a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
{{--                @can('تجارب المستخدمين')--}}
                    <li class="nav-item"><a href=""><i class="la la-users"></i>
                            <span class="menu-title" data-i18n="nav.dash.main">تجارب المستخدمين </span>
                            <span
                                class="badge badge badge-info badge-pill float-right mr-2">{{App\Models\UserExperience::count()}}</span>
                        </a>
                        <ul class="menu-content">
{{--                            @can('عرض التجارب')--}}
                                <li><a class="menu-item" href="{{url('Admin/userExperience')}}"
                                       data-i18n="nav.dash.ecommerce"> عرض التجارب</a>
                                </li>
{{--                            @endcan--}}
{{--                            @can('إضافة تجربة جديدة')--}}
                                <li><a class="menu-item" href="{{url('Admin/userExperience/create')}}" data-i18n="nav.dash.crypto">
                                        إضافة تجربة جديدة </a>
                                </li>
{{--                            @endcan--}}
                        </ul>
                    </li>
{{--                @endcan--}}
                {{--                @can('الشركاء')--}}
                <li class="nav-item"><a href=""><i class="la la-user-secret"></i>
                        <span class="menu-title" data-i18n="nav.dash.main">الشركاء </span>
                        <span
                            class="badge badge badge-info badge-pill float-right mr-2">{{App\Models\Partner::count()}}</span>
                    </a>
                    <ul class="menu-content">
                        {{--                            @can('عرض الشركاء')--}}
                        <li><a class="menu-item" href="{{url('Admin/partners')}}"
                               data-i18n="nav.dash.ecommerce"> عرض الشركاء</a>
                        </li>
                        {{--                            @endcan--}}
                        {{--                            @can('إضافة شريك جديد')--}}
                        <li><a class="menu-item" href="{{url('Admin/partners/create')}}" data-i18n="nav.dash.crypto">
                                إضافة شريك جديد </a>
                        </li>
                        {{--                            @endcan--}}
                    </ul>
                </li>
                {{--                @endcan--}}
            @can('حول الشركة')
            <li class="nav-item">
                <a href="{{url('Admin/about')}}"><i class="la la-bullhorn"></i>
                    <span class="menu-title" data-i18n="nav.dash.main"> حول الشركة </span>
                </a>
            </li>
            @endcan
            @can('معلومات التواصل')
            <li class="nav-item">
                <a href="{{url('Admin/contact_info')}}"><i class="la la-comments"></i>
                    <span class="menu-title" data-i18n="nav.dash.main"> معلومات التواصل </span>
                </a>
            </li>
            @endcan
{{--            @can('معلومات التواصل')--}}
                <li class="nav-item">
                    <a href="{{url('Admin/getMap')}}"><i class="la la-map-marker"></i>
                        <span class="menu-title" data-i18n="nav.dash.main"> الموقع الجغرافي </span>
                    </a>
                </li>
{{--            @endcan--}}

        </ul>
    </div>
</div>

<!--End Sidebare-->
