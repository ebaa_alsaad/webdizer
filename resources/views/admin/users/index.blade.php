﻿@extends('layouts.admin')
@section('title')
    | المستخدمين
@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('admin\dashboard')}}">الرئيسية</a>
                                </li>
                                <li class="breadcrumb-item active"> قائمة المستخدمين
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- DOM - jQuery events table -->
                <section id="dom">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i
                                            class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        </ul>
                                    </div>
                                </div>

                                @include('admin.includes.alerts.success')
                                @include('admin.includes.alerts.errors')

                        <div class="card-content collapse show">
                            <div class="card-body card-dashboard" style="text-align: center">
                                <table
                                    class="table display nowrap table-striped table-bordered ">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">اسم المستخدم</th>
                                        <th scope="col">البريد الالكتروني</th>
                                        <th scope="col">نوع المستخدم</th>
                                        <th scope="col">حالة المستخدم</th>
                                        <th scope="col">العمليات</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @isset($data)
                                        @foreach ($data as $key => $user)
                                            <tr >
                                                <td>{{ ++$key }}</td>
                                                <td>{{ $user->name }}</td>
                                                <td>{{ $user->email }}</td>
                                                <td>
                                                    @if (!empty($user->getRoleNames()))
                                                        @foreach ($user->getRoleNames() as $v)
                                                            <label>{{ $v }}</label>
                                                        @endforeach
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($user->status == '1')
                                                        <span class="badge badge-glow badge-pill badge-border border-success success">
                                                        <i class="la la-check-circle"></i>
                                                        {{ $user->getStatus() }}
                                                      </span>

                                                    @else
                                                    <span class="badge badge-glow badge-pill badge-border border-warning warning">
                                                     <i class="la la-circle"></i>
                                                         {{ $user->getStatus() }}
                                                    </span>
                                                        </span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @can('تعديل مستخدم')
                                                        <a href="{{ route('users.edit', $user->id) }}" class="btn btn-sm btn-outline-info box-shadow-1"
                                                           title="تعديل"><i class="la la-pencil-square"></i>تعديل </a>
                                                    @endcan

                                                    @can('حذف مستخدم')

                                                        <button class=" btn btn-sm btn-outline-danger box-shadow-1" data-delid={{$user->id}} data-toggle="modal" data-target="#delete"
                                                           title="حذف" >حذف<i class="la la-trash"></i></button>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endisset

                                            </tbody>
                                        </table>

                                {{$data->links('admin.pagination')}}
                                    </div>

                                </div>
                            </div>
                        </div>

                        @isset($user)
                        <div class="modal fade" id="delete" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-sm" role="document">>
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-hidden="true">&times;</button>
                                    </div>

                                    <!-- Modal -->
                                    <form action="{{url("Admin/users/{$user->id}")}}" method="post">
                                        {{method_field('DELETE')}}
                                        {{csrf_field()}}
                                        <div class="modal-body">
                                            <h4>   هل أنت متأكد من حذف هذا العنصر؟</h4>
                                            <input type="hidden" name="delet_id" id="del_id" value="">

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-success" data-dismiss="modal">إلغاء</button>
                                            <button type="submit" class="btn btn-warning">موافق</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endisset

                    </div>
                </section>

            </div>
        </div>
    </div>
@endsection
