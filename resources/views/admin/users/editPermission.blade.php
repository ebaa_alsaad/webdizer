@extends('layouts.admin')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('Admin/dashboard')}}">الرئيسية </a>
                                </li>
                                <li class="breadcrumb-item active">تعديل مجموعة صلاحيات
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i
                                            class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                @include('admin.includes.alerts.success')
                                @include('admin.includes.alerts.errors')
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form" action="{{route('role.update', $roles->id)}}" method="POST"
                                              enctype="multipart/form-data">
                                            @method('PATCH')
                                            @csrf
                                            <div class="form-body">
                                                <!-- row -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card mg-b-20">
                                                            <div class="card-body">
                                                                <div class="main-content-label mg-b-5">
                                                                    <div class="form-group">
                                                                        <p>اسم الصلاحية :</p>
                                                                        <input type="text"
                                                                               required
                                                                               id="name"
                                                                               value="{{$roles->name}}"
                                                                               class="form-control"
                                                                               placeholder="يرجى اختيار اسم لمجموعة الصلاحيات"
                                                                               name="name">
                                                                        @error("name")
                                                                        <span class="text-danger">{{$message}} </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <!-- col -->
                                                                    <div class="col-lg-4">
                                                                        <ul id="treeview1">
                                                                            <li><a href="#">الصلاحيات</a>
                                                                                <ul>
                                                                                    <li>
                                                                                        @foreach($permission as $value)
                                                                                            <label class="checkbox-inline" for="perm[{{ $value->id }}]">

                                                                                                <input id="perm[{{ $value->id }}]" type="checkbox" class="main name" style="margin-left: 5px;"
                                                                                                       name="permission[]"
                                                                                                       value="{{$value->id}}"
                                                                                                       @if($roles->permissions->contains($value->id)) checked=checked @endif/>
                                                                                                {{ $value->name }}</label>
                                                                                            <br>
                                                                                            </label>

                                                                                        @endforeach
                                                                                    </li>

                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- row closed -->

                                                <div class="form-actions">
                                                    <button type="button" class="btn btn-outline-warning box-shadow-1 mr-1"
                                                            onclick="history.back();">
                                                        <i class="ft-x"></i> تراجع
                                                    </button>
                                                    <button type="submit" class="btn btn-outline-primary box-shadow-1">
                                                        <i class="la la-check-square-o"></i> حفظ
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
@endsection
@section('script')


@endsection
