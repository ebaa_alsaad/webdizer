@extends('layouts.admin')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('Admin/dashboard')}}">الرئيسية </a>
                                </li>
                                <li class="breadcrumb-item active">إضافة مجموعة صلاحيات
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i
                                            class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                @include('admin.includes.alerts.success')
                                @include('admin.includes.alerts.errors')
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form" action="{{url('Admin/role')}}" method="POST"
                                              enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card mg-b-20">
                                                            <div class="card-body">
                                                                <div class="main-content-label mg-b-5">
                                                                    <div class="col-xs-7 col-sm-7 col-md-7">
                                                                        <div class="form-group">
                                                                            <p>اسم الصلاحية :</p>
                                                                            <div class="row">
                                                                                        <input type="text"
                                                                                               required
                                                                                               id="name"
                                                                                               class="form-control"
                                                                                               placeholder="يرجى اختيار اسم لمجموعة الصلاحيات"
                                                                                               name="name">
                                                                                        @error("name")
                                                                                        <span class="text-danger">{{$message}} </span>
                                                                                        @enderror
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <!-- col -->
                                                                    <div class="col-lg-4">
                                                                        <ul id="treeview1">
                                                                            <li><a href="#">الصلاحيات</a>
                                                                                <ul>
                                                                                    </li>
                                                                                    @foreach($permission as $value)
                                                                                        <label>

                                                                                            <input type="checkbox" class="main name" style="margin-left: 5px;"
                                                                                                   name="permission[]"
                                                                                                   value="{{$value->id}}"/>
                                                                                            {{ $value->name }}</label>
                                                                                            <br>
                                                                                        </label>
                                                                                        @endforeach
                                                                                        </li>

                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
{{--                                                <div class="row">--}}
{{--                                                    <div class="col-md-8">--}}
{{--                                                        <div class="form-group">--}}
{{--                                                            <label for="name">اسم المجموعة</label>--}}
{{--                                                            <input type="text"--}}
{{--                                                                   required--}}
{{--                                                                   id="name"--}}
{{--                                                                   class="form-control"--}}
{{--                                                                   placeholder="يرجى اختيار اسم لمجموعة الصلاحيات"--}}
{{--                                                                   name="name">--}}
{{--                                                            @error("name")--}}
{{--                                                            <span class="text-danger">{{$message}} </span>--}}
{{--                                                            @enderror--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="row skin skin-flat">--}}
{{--                                                    <div class="col-md-9 col-md-offset-3">--}}
{{--                                                        <ul >--}}
{{--                                                            <li class="parent">--}}
{{--                                                                <label><input type="checkbox" class="main" style="margin-left: 5px;" name="permission[]" value="المستخدمين"/></label><label for="node-4" class="expandable">المستخدمين<span class="caret"></span></label>--}}
{{--                                                                <ul>--}}
{{--                                                                    <li>--}}
{{--                                                                        <label><input type="checkbox" style="margin-left: 5px;" name="permission[]" value="إضافة مستخدم جديد|UserController@store|UserController@create'"/></label><label for="node-4-1">إضافة مستخدم جديد </label>--}}
{{--                                                                    </li>--}}
{{--                                                                    <li>--}}
{{--                                                                        <label><input type="checkbox" style="margin-left: 5px;" name="permission[]" value='UserController@index|UserController@update|عرض الكل|UserController@edit|UserController@destroy'/></label><label for="node-4-1">تعديل إعلان </label>--}}
{{--                                                                    </li>--}}
{{--                                                                    <li>--}}
{{--                                                                        <label><input type="checkbox" style="margin-left: 5px;" name="permission[]" value="صلاحيات المستخدم|RoleController@store|RoleController@create'"/></label><label for="node-4-1">صلاحيات المستخدم</label>--}}
{{--                                                                    </li>--}}
{{--                                                                    <li>--}}
{{--                                                                        <label><input type="checkbox" style="margin-left: 5px;" name="permission[]" value='RoleController@index|RoleController@updaterole|إضافة مجموعة صلاحيات|RoleController@edit|RoleController@destroy'/></label><label for="node-4-1">إضافة مجموعة صلاحيات</label>--}}
{{--                                                                    </li>--}}
{{--                                                                </ul>--}}
{{--                                                            </li>--}}
{{--                                                            <li class="parent">--}}
{{--                                                                <label><input type="checkbox" class="main" style="margin-left: 5px;" name="permission[]" value="الخدمات"/></label><label for="node-4" class="expandable">الخدمات<span class="caret"></span></label>--}}
{{--                                                                <ul>--}}
{{--                                                                    <li>--}}
{{--                                                                        <label><input type="checkbox" style="margin-left: 5px;" name="permission[]" value="ServiceController@create|ServiceController@store|إضافة خدمة جديدة"/></label><label for="node-4-1">إضافة خدمة جديدة </label>--}}
{{--                                                                    </li>--}}
{{--                                                                    <li>--}}
{{--                                                                        <label><input type="checkbox" style="margin-left: 5px;" name="permission[]" value="تعديل خدمة|ServiceController@index|ServiceController@update|ServiceController@edit|ServiceController@destroy"/></label><label for="node-4-1">تعديل خدمة </label>--}}
{{--                                                                    </li>--}}
{{--                                                                </ul>--}}
{{--                                                            </li>--}}
{{--                                                            <li class="parent">--}}
{{--                                                                <label><input type="checkbox" class="main" style="margin-left: 5px;" name="permission[]" value="المشاريع"/></label><label for="node-0" class="expandable">المشاريع<span class="caret"></span></label>--}}
{{--                                                                <ul>--}}
{{--                                                                    <li>--}}
{{--                                                                        <label><input type="checkbox" style="margin-left: 5px;" name="permission[]" value="إضافة مشروع جديد|ProjectController@create|ProjectController@store"/></label><label for="node-4-1">إضافة مشروع جديد </label>--}}
{{--                                                                    </li>--}}
{{--                                                                    <li>--}}
{{--                                                                        <label><input type="checkbox" style="margin-left: 5px;" name="permission[]" value="ProjectController@index|ProjectController@update|تعديل مشروع|ProjectController@edit|ProjectController@destroy"/></label><label for="node-4-1">تعديل مشروع </label>--}}
{{--                                                                    </li>--}}
{{--                                                                </ul>--}}
{{--                                                            </li>--}}
{{--                                                            <li class="parent">--}}
{{--                                                                <label><input type="checkbox" class="main" style="margin-left: 5px;" name="permission[]" value="الأخبار"/></label><label for="node-4" class="expandable">الأخبار<span class="caret"></span></label>--}}
{{--                                                                <ul>--}}
{{--                                                                    <li>--}}
{{--                                                                        <label><input type="checkbox" style="margin-left: 5px;" name="permission[]" value="NewsController@create|NewsController@store|إضافة خبر جديد"/></label><label for="node-4-1">إضافة خبر جديد </label>--}}
{{--                                                                    </li>--}}
{{--                                                                    <li>--}}
{{--                                                                        <label><input type="checkbox" style="margin-left: 5px;" name="permission[]" value="تعديل خبر|NewsController@index|NewsController@update|NewsController@edit|NewsController@destroy"/></label><label for="node-4-1">تعديل خبر</label>--}}
{{--                                                                    </li>--}}
{{--                                                                </ul>--}}
{{--                                                            </li>--}}
{{--                                                            <li class="parent">--}}
{{--                                                                <label><input type="checkbox" class="main" style="margin-left: 5px;" name="permission[]" value="فريق العمل"/></label><label for="node-0" class="expandable">فريق العمل<span class="caret"></span></label>--}}
{{--                                                                <ul>--}}
{{--                                                                    <li>--}}
{{--                                                                        <label><input type="checkbox" style="margin-left: 5px;" name="permission[]" value="إضافة جديد|TeamController@create|TeamController@store"/></label><label for="node-4-1">إضافة جديد</label>--}}
{{--                                                                    </li>--}}
{{--                                                                    <li>--}}
{{--                                                                        <label><input type="checkbox" style="margin-left: 5px;" name="permission[]" value="TeamController@index|TeamController@update|تعديل|TeamController@edit|TeamController@destroy"/></label><label for="node-4-1"> تعديل</label>--}}
{{--                                                                    </li>--}}
{{--                                                                </ul>--}}
{{--                                                            </li>--}}
{{--                                                            <li class="parent">--}}
{{--                                                                <label><input type="checkbox" class="main" style="margin-left: 5px;" name="permission[]" value="التصنيفات"/></label><label for="node-4" class="expandable">التصنيفات<span class="caret"></span></label>--}}
{{--                                                                <ul>--}}
{{--                                                                    <li>--}}
{{--                                                                        <label><input type="checkbox" style="margin-left: 5px;" name="permission[]" value="CategoryController@create|CategoryController@store|إضافة تصنيف "/></label><label for="node-4-1">إضافة تصنيف  </label>--}}
{{--                                                                    </li>--}}
{{--                                                                    <li>--}}
{{--                                                                        <label><input type="checkbox" style="margin-left: 5px;" name="permission[]" value="تعديل تصنيف|CategoryController@index|CategoryController@update|CategoryController@edit|CategoryController@destroy"/></label><label for="node-4-1">تعديل تصنيف</label>--}}
{{--                                                                    </li>--}}

{{--                                                                </ul>--}}
{{--                                                            </li>--}}
{{--                                                            <li class="parent">--}}
{{--                                                                <label><input type="checkbox" class="main" style="margin-left: 5px;" name="permission[]" value="سلايدر الصفحة الرئيسية"/></label><label for="node-0" class="expandable">سلايدر الصفحة الرئيسية<span class="caret"></span></label>--}}
{{--                                                                <ul>--}}
{{--                                                                    <li>--}}
{{--                                                                        <label><input type="checkbox" style="margin-left: 5px;" name="permission[]" value="  إضافة Slider جديد|SliderController@create|SliderController@store"/></label><label for="node-4-1">إضافة Slider جديد</label>--}}
{{--                                                                    </li>--}}
{{--                                                                    <li>--}}
{{--                                                                        <label><input type="checkbox" style="margin-left: 5px;" name="permission[]" value="SliderController@index|SliderController@update| تعديل Slider|SliderController@edit|SliderController@destroy"/></label><label for="node-4-1">  تعديل Slider</label>--}}
{{--                                                                    </li>--}}
{{--                                                                </ul>--}}
{{--                                                            </li>--}}
{{--                                                            <li class="parent">--}}
{{--                                                                <label><input type="checkbox" class="main" style="margin-left: 5px;" name="permission[]" value="VariableController@getAbout|VariableController@setAbout| حول الشركة|VariableController@create"/></label><label for="node-0" class="expandable">حول الشركة<span class="caret"></span></label>--}}
{{--                                                            </li>--}}
{{--                                                            <li class="parent">--}}
{{--                                                                <label><input type="checkbox" class="main" style="margin-left: 5px;" name="permission[]" value="VariableController@getInfo| معلومات التواصل|VariableController@setInfo"/></label><label for="node-0" class="expandable">معلومات التواصل<span class="caret"></span></label>--}}
{{--                                                            </li>--}}
{{--                                                        </ul>--}}
{{--                                                    </div>--}}

{{--                                                </div>--}}

                                                <div class="form-actions">
                                                    <button type="button" class="btn btn-outline-warning box-shadow-1 mr-1"
                                                            onclick="history.back();">
                                                        <i class="ft-x"></i> تراجع
                                                    </button>
                                                    <button type="submit" class="btn btn-outline-primary box-shadow-1">
                                                        <i class="la la-check-square-o"></i> حفظ
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
@endsection
