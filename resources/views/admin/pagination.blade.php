@if ($paginator->hasPages())
    <nav aria-label="Page navigation">
        <ul class="pagination justify-content-center pagination-separate pagination-round pagination-sm">
            @if ($paginator->onFirstPage())
            <li class="page-item">
                <a class="page-link" aria-label="Previous">
                    <span aria-hidden="true">« السابق</span></a>
            </li>
            @else
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" aria-label="Previous">
                    <span>السابق</span>
                </a>
            </li>
            @endif
            @foreach ($elements as $element)
                @if (is_string($element))
                    <li class="page-item"><a class="page-link" href="#">{{ $element }}</a></li>
                @endif
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active"><a class="page-link" href="#">{{ $page }}</a></li>
                        @else
                            <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach
                @if ($paginator->hasMorePages())
                    <li class="page-item">
                        <a class="page-link" aria-label="Next">
                            <span aria-hidden="true">التالي »</span></a>
                    </li>

                @else
                    <li class="page-item">
                        <a class="page-link" href="{{ $paginator->nextPageUrl() }}" aria-label="Next">
                            <span>التالي</span>
                        </a>
                    </li>
                @endif
        </ul>
    </nav>

@endif

{{--    <ul class="pager">--}}

{{--        @if ($paginator->onFirstPage())--}}
{{--            <li class="disabled"><span>← Previous</span></li>--}}
{{--        @else--}}
{{--            <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev">← Previous</a></li>--}}
{{--        @endif--}}

{{--        @foreach ($elements as $element)--}}

{{--            @if (is_string($element))--}}
{{--                <li class="disabled"><span>{{ $element }}</span></li>--}}
{{--            @endif--}}



{{--            @if (is_array($element))--}}
{{--                @foreach ($element as $page => $url)--}}
{{--                    @if ($page == $paginator->currentPage())--}}
{{--                        <li class="active my-active"><span>{{ $page }}</span></li>--}}
{{--                    @else--}}
{{--                        <li><a href="{{ $url }}">{{ $page }}</a></li>--}}
{{--                    @endif--}}
{{--                @endforeach--}}
{{--            @endif--}}
{{--        @endforeach--}}



{{--        @if ($paginator->hasMorePages())--}}
{{--            <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">Next →</a></li>--}}
{{--        @else--}}
{{--            <li class="disabled"><span>Next →</span></li>--}}
{{--        @endif--}}
{{--    </ul>--}}
{{--@endif--}}
