﻿@extends('layouts.admin')
@section('title')
    | المشاريع
@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('admin\dashboard')}}">الرئيسية</a>
                                </li>
                                <li class="breadcrumb-item active"> قائمة المشاريع
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- DOM - jQuery events table -->
                <section id="dom">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i
                                            class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        </ul>
                                    </div>
                                </div>

                                @include('admin.includes.alerts.success')
                                @include('admin.includes.alerts.errors')

                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard" style="text-align: center">
                                        <table
                                            class="table display nowrap table-striped table-bordered ">
                                            <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">الاسم AR</th>
                                                <th scope="col">الاسم EN</th>
                                                <th scope="col"> الشرح AR</th>
                                                <th scope="col"> الشرح EN</th>
                                                <th scope="col"> التصنيف </th>
                                                <th scope="col"> الصورة </th>
                                                <th scope="col">العمليات</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                    @isset($projects)
                                        @foreach ($projects as $key => $project)
                                            <tr>
                                                <td>{{ ++$key }}</td>
                                                <td style="width: 114px">{{ $project->{'title:ar'} }}</td>
                                                <td style="width: 114px">{{ $project->{'title:en'} }}</td>
                                                <td style="width: 100px">
                                                    <?php $text = strip_tags( $project->{'text:ar'});
                                                    $new = trim($text);
                                                    echo substr($new,0,50);?></td>
                                                <td style="width: 100px">
                                                    <?php $text = strip_tags($project->{'text:en'});
                                                    $new = trim($text);
                                                    echo substr($new,0,50);?></td>
                                                <td style="width: 114px">
                                                    @isset($project->Category->{'name:ar'})
                                                        {{ $project->Category->{'name:ar'} }}
                                                    @else
                                                         لا يوجد
                                                    @endisset
                                                </td>
                                                <td><img width="150px" height="100px"  src="{{$project->image}}"></td>

                                                <td style="width: 195px">
                                                    @can('تعديل مشروع')
                                                        <a href="{{ route('project.edit', $project->id) }}" class="btn btn-sm btn-outline-info box-shadow-1"
                                                           title="تعديل"><i class="la la-pencil-square"></i>تعديل </a>
                                                    @endcan

                                                    @can('حذف مشروع')

                                                    <button class=" btn btn-sm btn-outline-danger box-shadow-1" data-delid={{$project->id}} data-toggle="modal" data-target="#delete"
                                                            title="حذف" >
                                                        <i class="la la-trash"></i>حذف </button>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endisset

                                            </tbody>
                                        </table>
                                        {{$projects->links('admin.pagination')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        @isset($project)
                        <div class="modal fade" id="delete" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-sm" role="document">>
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-hidden="true">&times;</button>
                                    </div>

                                    <!-- Modal -->
                                    <form action="{{url("Admin/project/{$project->id}")}}" method="post">
                                        {{method_field('DELETE')}}
                                        {{csrf_field()}}
                                        <div class="modal-body">
                                            <h4>   هل أنت متأكد من حذف هذا العنصر؟</h4>
                                            <input type="hidden" name="delet_id" id="del_id" value="">

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-success" data-dismiss="modal">إلغاء</button>
                                            <button type="submit" class="btn btn-warning">موافق</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endisset
                    </div>
                </section>

            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('admin/js/scripts/delete.js')}}" type="text/javascript"></script>
@endsection
