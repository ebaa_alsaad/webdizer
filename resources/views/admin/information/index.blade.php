@extends('layouts.admin')
@section('title')
    | بيانات معلومات التواصل
@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('Admin/dashboard')}}">الرئيسية </a>
                                </li>
                                <li class="breadcrumb-item active">بيانات معلومات التواصل
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i
                                            class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div id="response-message"></div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form id="signupForm" class="form" action="{{url('Admin/edit_contact_info')}}" method="POST"
                                              enctype="multipart/form-data">
                                            @csrf
                                            <input hidden name="_method" value="POST" >

                                            <div class="form-body">
                                                <div class="row "style="margin: 1.5rem;">
                                                    <label class=" col-md-2" for="email" style="text-align: left;margin: auto;"> E-mail </label>
                                                    <div class="col-md-10 input-group">
                                                        <input class="form-control "
                                                               id="email"
                                                               name="email"
                                                               type="text"
                                                               value="{{$variable[0]->getValue('email')}}"
                                                               oldData="{{$variable[0]->getValue('email')}}" disabled/>
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-success" type="button">تعديل</button>
                                                        </span>
                                                    </div>
                                                    @error("email")
                                                    <span class="text-danger">{{$message}} </span>
                                                    @enderror
                                                </div>
                                                <div class="row" style="margin: 1.5rem;">
                                                    <label class=" col-md-2" for="phone" style="text-align: left;margin: auto;"> Phone_Number </label>

                                                    <div class="col-md-10 input-group">
                                                        <input class="form-control "
                                                               id="phone"
                                                               name="phone"
                                                               type="text"
                                                               value="{{$variable[0]->getValue('phone')}}"
                                                               oldData="{{$variable[0]->getValue('phone')}}" disabled/>
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-success" type="button">تعديل</button>
                                                        </span>
                                                    </div>
                                                    @error("phone")
                                                    <span class="text-danger">{{$message}} </span>
                                                    @enderror
                                                </div>
                                                <div class="row "style="margin: 1.5rem;">
                                                    <label class=" col-md-2" for="en" style="text-align: left;margin: auto;"> Address En </label>

                                                    <div class="col-md-10 input-group">
                                                        <input class="form-control "
                                                               id="en"
                                                               name="address_en"
                                                               type="text"
                                                               value="{{$variable[0]->getValueADR('address','en')}}"
                                                               oldData="{{$variable[0]->getValueADR('address','en')}}" disabled/>
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-success" type="button">تعديل</button>
                                                            </span>
                                                    </div>
                                                    @error("address_en")
                                                    <span class="text-danger">{{$message}} </span>
                                                    @enderror
                                                </div>
                                                <div class="row "style="margin: 1.5rem;">
                                                    <label class=" col-md-2" for="ar" style="text-align: left;margin: auto;"> Address Ar </label>

                                                    <div class="col-md-10 input-group">
                                                        <input class="form-control "
                                                               id="ar"
                                                               name="address_ar"
                                                               type="text"
                                                               value="{{$variable[0]->getValueADR('address','ar')}}"
                                                               oldData="{{$variable[0]->getValueADR('address','ar')}}" disabled/>
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-success" type="button">تعديل</button>
                                                            </span>
                                                    </div>
                                                    @error("address_ar")
                                                    <span class="text-danger">{{$message}} </span>
                                                    @enderror
                                                </div>
                                                <div class="row "style="margin: 1.5rem;">
                                                        <label class=" col-md-2" for="facebook" style="text-align: left;margin: auto;"> Facebook </label>

                                                        <div class="col-md-10 input-group">
                                                            <input class="form-control "
                                                                   id="facebook"
                                                                   name="facebook"
                                                                   type="text"
                                                                   value="{{$variable[0]->getValue('facebook')}}"
                                                                   oldData="{{$variable[0]->getValue('facebook')}}" disabled/>
                                                            <span class="input-group-btn">
                                                            <button class="btn btn-success" type="button">تعديل</button>
                                                            </span>
                                                        </div>
                                                        @error("facebook")
                                                         <span class="text-danger">{{$message}} </span>
                                                        @enderror
                                                </div>

                                                <div class="row "style="margin: 1.5rem;">
                                                    <label class=" col-md-2" for="whatsapp" style="text-align: left;margin: auto;"> WhatsApp </label>

                                                    <div class="col-md-10 input-group">
                                                        <input class="form-control "
                                                               id="whatsapp"
                                                               name="whatsup"
                                                               type="number"
                                                               value="{{$variable[0]->getValue('whatsup')}}"
                                                               oldData="{{$variable[0]->getValue('whatsup')}}" disabled/>
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-success" type="button">تعديل</button>
                                                        </span>
                                                    </div>
                                                    @error("whatsup")
                                                    <span class="text-danger">{{$message}} </span>
                                                    @enderror
                                                </div>
                                                    <div class="row "style="margin: 1.5rem;">
                                                    <label class=" col-md-2" for="instagram" style="text-align: left;margin: auto;"> Instagram </label>

                                                    <div class="col-md-10 input-group">
                                                        <input class="form-control "
                                                               id="instagram"
                                                               name="instagram"
                                                               type="text"
                                                               value="{{$variable[0]->getValue('instagram')}}"
                                                               oldData="{{$variable[0]->getValue('instagram')}}" disabled/>
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-success" type="button">تعديل</button>
                                                            </span>
                                                    </div>
                                                    @error("instagram")
                                                    <span class="text-danger">{{$message}} </span>
                                                    @enderror
                                                </div>
                                                    <div class="row "style="margin: 1.5rem;">
                                                    <label class=" col-md-2" for="twitter" style="text-align: left;margin: auto;"> Twitter </label>

                                                    <div class="col-md-10 input-group">
                                                        <input class="form-control "
                                                               id="twitter"
                                                               name="twitter"
                                                               type="text"
                                                               value="{{$variable[0]->getValue('twitter')}}"
                                                               oldData="{{$variable[0]->getValue('twitter')}}" disabled/>
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-success" type="button">تعديل</button>
                                                            </span>
                                                    </div>
                                                    @error("twitter")
                                                    <span class="text-danger">{{$message}} </span>
                                                    @enderror
                                                </div>
                                                    <div class="row "style="margin: 1.5rem;">
                                                    <label class=" col-md-2" for="linkedin" style="text-align: left;margin: auto;"> Linkedin </label>

                                                    <div class="col-md-10 input-group">
                                                        <input class="form-control "
                                                               id="linkedin"
                                                               name="linkedin"
                                                               type="text"
                                                               value="{{$variable[0]->getValue('linkedin')}}"
                                                               oldData="{{$variable[0]->getValue('linkedin')}}" disabled/>
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-success" type="button">تعديل</button>
                                                            </span>
                                                    </div>
                                                    @error("linkedin")
                                                    <span class="text-danger">{{$message}} </span>
                                                    @enderror
                                                </div>
                                                <div class="row "style="margin: 1.5rem;">
                                                    <label class=" col-md-2" for="youtube" style="text-align: left;margin: auto;"> Youtube </label>

                                                    <div class="col-md-10 input-group">
                                                        <input class="form-control "
                                                               id="youtube"
                                                               name="youtube"
                                                               type="text"
                                                               value="{{$variable[0]->getValue('youtube')}}"
                                                               oldData="{{$variable[0]->getValue('youtube')}}" disabled/>
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-success" type="button">تعديل</button>
                                                            </span>
                                                    </div>
                                                    @error("youtube")
                                                    <span class="text-danger">{{$message}} </span>
                                                    @enderror
                                                </div>


                                                <div class="form-actions">
                                                    @can('معلومات التواصل')
                                                    <button type="button" class="btn btn-outline-warning box-shadow-1 mr-1"
                                                            onclick="history.back();">
                                                        <i class="ft-x"></i> تراجع
                                                    </button>
                                                    @endcan
                                                    @can('معلومات التواصل')
                                                    <button type="button" class="btn btn-outline-primary box-shadow-1">
                                                        <i class="la la-check-square-o"></i> تعديل
                                                    </button>
                                                    @endcan
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {

            $(document).on('click', '.btn-success', function () {
                var sp=$(this).parent();
                sp.prev().removeAttr("disabled");
                sp.prev().addClass('change');
                $(this).removeClass('btn-success');
                $(this).addClass('btn-danger');
                $(this).text("إلغاء");
            });

            $(document).on('click', '.btn-danger', function () {
                var sp=$(this).parent();
                sp.prev().attr('disabled','disabled');
                sp.prev().removeClass('change');
                var oldVal=sp.prev().attr('oldData');
                sp.prev().val(oldVal);
                $(this).removeClass('btn-danger');
                $(this).addClass('btn-success');
                $(this).text("تعديل");
            });
            $(document).on('click', '.btn-outline-primary', function () {
                var data ="_token="+$('[name="_token"]').val()+ "&_method=POST";

                var data1='';
                $('.change').each(function() {
                    console.log($(this).val());
                    if($(this).val()==null||$(this).val()=='')
                        data1=data1+"&"+$(this).attr("name")+"="+'*';
                    else
                        data1=data1+"&"+$(this).attr("name")+"="+$(this).val();

                })
                if(data1!='') {
                    // $('#error').addClass('hide');
                    var url = $('#signupForm').attr('action');
                    data=data+data1;
                    console.log(data);
                    $.post(url, data, function (res) {
                        console.log('M',res);

                        if(res.success == true ){
                            $('#response-message').empty();
                            $('#response-message').append(
                                '<br><div class="row mr-2 ml-2">\n' +
                                '        <button type="text" class="btn btn-lg btn-block btn-outline-success mb-2"\n' +
                                '                id="type-error">'+res.msg+'\n' +
                                '        </button>\n' +
                                '    </div>'
                            );

                            new PNotify({
                                title: "تم",
                                text: res.msg,
                                type: 'success',
                                hideAfter:4000,
                                styling: 'bootstrap3',
                            });

                            $('.change').each(function () {
                                $(this).attr('oldData',$(this).val());
                                $(this).attr('disabled','disabled');
                                var sp=$(this).next('span');

                                sp.find('button').removeClass('btn-danger');
                                sp.find('button').addClass('btn-success');
                                sp.find('button').text("تعديل")
                            })

                        }
                        else{
                            $('#response-message').empty();
                            $('#response-message').append(
                                '    <div class="row mr-2 ml-2" >\n' +
                                '        <button type="text" class="btn btn-lg btn-block btn-outline-danger mb-2"\n' +
                                '                id="type-error">'+res.msg+'\n' +
                                '        </button>\n' +
                                '    </div>\n'
                            );
                            new PNotify({
                                title:res.title,
                                text: res.msg,
                                type: 'error',
                                hideAfter:4000,
                                styling: 'bootstrap3'
                            });
                        }
                    },'json');
                }else {
                    new PNotify({
                        title:'',
                        text: ' لا يوجد حقل تم تغييره',
                        type: 'error',
                        hideAfter:4000,
                        styling: 'bootstrap3'
                    });

                }
            })
        })
    </script>

@endsection
