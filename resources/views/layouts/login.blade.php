<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title> تسجيل الدخول للوحة التحكم </title>

    <meta content="" name="description" />
    <link rel="shortcut icon" href="{{asset('/assets/images/favicon.ico')}}">

    <link href="{{asset('/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Cairo&display=swap" rel="stylesheet">

    <style>
        body {
            font-family: 'Cairo', sans-serif;
        }

        .wrapper-page {
            margin: 3.5% auto;
        }
        .card-body {
            padding: 0 1.25rem 1.25rem;
        }
    </style>
</head>

<body>
<!-- Begin page -->
<div id="wrapper">

@yield('content')

</div>
<!-- BEGIN VENDOR JS-->
<script src="{{asset('/assets/js/jquery.min.js')}}"></script>
<script src="{{asset('/assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('/assets/js/jquery.slimscroll.js')}}"></script>

</body>
</html>

